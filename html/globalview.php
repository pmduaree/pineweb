<?php
//////////////////////////////////////////////////////////////////////////////
// globalview.php
//
// This is a rewrite of Amit Nair's code which was too complex and stuff.
//
// Author: Iztok Pizorn <iztok.p@oleum.com>
//
//////////////////////////////////////////////////////////////////////////////
$client = new SoapClient('https://webapi.gvsi.com/services/mv2?wsdl');

if (!isset($_GET['instrument_id'])) {
    print '<html>';
    print '<body>';
    print '</body>';
    print '</html>';
    die();
}

$instrument_id = $_REQUEST['instrument_id'];
$exchange_code = $_REQUEST['exchange_code'];
$start_date = $_REQUEST['start_date'];
$end_date = $_REQUEST['end_date'];
$interval = $_REQUEST['timestamp'];

$tags = '<TAGS>
        <TAG TAG_NAME="INSTRUMENT_ID" />
        <TAG TAG_NAME="OPEN" />
        <TAG TAG_NAME="HIGH" />
        <TAG TAG_NAME="LOW" />
        <TAG TAG_NAME="CLOSE" />
        <TAG TAG_NAME="VOLUME" />
        <TAG TAG_NAME="TRADE_DATETIME" />
        </TAGS>';

$requestXML = '';
$requestXML .= '<API_REQUEST>';
$requestXML .= '<INSTRUMENTS>';
$requestXML .= sprintf('<INSTRUMENT INSTRUMENT_ID="%s"/>', $instrument_id);
$requestXML .= '</INSTRUMENTS>';
$exchange_cond = $exchange_code ? sprintf('EXCHANGE_CODE="%s"',$exchange_code) : '';
$requestXML .= sprintf('<CONDITIONS %s START_DATE="%s" END_DATE="%s" BAR_INTERVAL="%d" />',
                       $exchange_cond, $start_date, $end_date, $interval);
$requestXML .= $tags;
$requestXML .= '</API_REQUEST>';


$data = $client->getIntradayPrices("concordapi","energy",$requestXML);



$xmlIterator = new SimpleXmlIterator($data);
$tablefirst = array();
$arr= array();
$heading= array();
$table1 = "<table class='table'>";
$table = "";
for($xmlIterator->rewind(); $xmlIterator->valid(); $xmlIterator->next() ) {
    if($xmlIterator->getChildren()->count() > 0) {
        foreach($xmlIterator->getChildren() as $name => $data) {
            $tablefirst= array();
            foreach($data->attributes() as $a => $b) {
                $heading[] = $a;
                $tablefirst[$a] = (string) $b;					
            }
            $arr[] = $tablefirst;
        }
	}
}

$heading = array_unique($heading);

$csv_str = '';
$csv_arr = array();
$row = 0;
foreach($heading as $key => $value)
{
    $csv_arr[] = $value;
}

$json[] = $csv_arr;
//fputcsv($fp, $csv_arr, ',', '"');
//$csv_str .= implode(',',$csv_arr);
if(is_array($arr) && count($arr)>0)
{
    foreach($arr as $k1 => $v1)
    {
        $csv_arr = array();
        foreach($heading as $k2 => $v2)
        {
            if(isset($v1[$v2]))
            {
                $csv_arr[] = $v1[$v2];
            }
            else
            {
                $csv_arr[] = "";
            }
        }
        
        $json[] = $csv_arr;
        //fputcsv($fp, $csv_arr, ',', '"');
        //$csv_str .= "\n".implode(',',$csv_arr);
    }
}
echo json_encode($json);
exit;



?>
