<?php

/**
 *
 * Various methods to generate JavaScript.
 * @author vbotalov
 *
 */
class JSHelper {

	/**
	 *
	 * Generate jQuery object.
	 * @param String $selector - DOM selector
	 */
	public static function generateJQueryObj($selector) {
		$result = "";
		$result = $result."$(".$selector.")";
		return $result;
	}

	/**
	 *
	 * Generate object method call
	 * @param string $obj - name of object
	 * @param string $method - name of object method
	 * @param string $paramStr - string with all params
	 */
	public static function generateCall($obj, $method, $paramStr) {
		$result = "";
		$result = $result.$obj.".".$method."(".$paramStr.")";
		return $result;
	}

	/**
	 *
	 * Generate object with its properties.
	 * @param array $array - array with object properties.
	 */
	public static function generateObj($array) {
		$result = "{";
		foreach ($array as $key => $value) {
			$result = $result.$key.": ".$value.", ";
		}
		$result = $result."}";
		return $result;
	}

	/**
	 *
	 * Generate jQuery call after DOM is ready.
	 * @param string $callback - code of callback called after DOM is ready.
	 */
	public static function generateReady($callback) {
		$result = "$(document).ready(function() {\n".$callback."\n} );";

		return $result;
	}

	/**
	 *
	 * Generate jQuery call after DOM is ready.
	 * @param string $callback - code of callback called after DOM is ready.
	 */
	public static function generateLoaded($callback) {
		$result = "$(window).load(function() {\n".$callback."\n} );";

		return $result;
	}

	/**
	 *
	 * Generate JavaScript array with 2D array data.
	 * @param array $data - 2D array with data.
	 */
	public static function generate2DArray($data) {
		array_multisort($data);
		$result = "";
		$result = $result."[";
		foreach ($data as $row) {
			$result = $result."[";
			foreach ($row as $column) {
				$result = $result.$column.", ";
			}
			$result = $result."],\n";
		}
		$result = $result."]";

		return $result;
	}
	/**
	 *
	 * Generate parameter string from parameter array
	 * @param array $params - array of parameters.
	 */
	public static function generateParamsStr($params) {
		$result = "";
		foreach ($params as $param) {
			$result = $result.$param.", ";
		}
		$result = substr($result, 0, strlen($result) - 2);
		return $result;
	}

}

?>
