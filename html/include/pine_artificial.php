<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="/jquery/jquery-ui-1.10.4.css">
<link rel="stylesheet" type="text/css" href="/css/oleum.css">
<script src="/jquery/jquery-1.10.2.js"></script>
<script src="/jquery/jquery-ui-1.10.4.js"></script>
<script type="text/javascript" src="http://code.highcharts.com/stock/highstock.js"></script>
<script type="text/javascript" src="http://code.highcharts.com/stock/modules/exporting.js"></script>
<script>function RefreshParent() { $("#sub_frame").load("/include/ticks.php"); } </script>
</head>

<body>


<div id="current_assets">

<style>
    .disabled_asset_row { color: #999999 ; }
</style>
<?php
require_once('sql_db.php');
?>

<h2> Artificial Data </h2>
<div id="asset_table_container">
<table>
<tr>
<th>Id</th>
<th style="width:70px">Name</th>
<th style="width:370px">Description</th>
<th style="width:160px">Last Data</th>
<th>Close</th>
<th></th>
</tr>
<?php

function DurationToString($dur)
{
    $tz = new DateTimeZone('UTC');
    $td1 = new DateTime("now", $tz);
    $td2 = new DateTime("now", $tz);
    //print_r($td1);
    $td1 = $td1->sub($dur);
    $td2 = $td2->sub(new DateInterval('PT66S'));
    //print_r($td1);
    //print_r($td2);
    //die();
    if ($td1 > $td2) return 'just now';
    $S = array();
    if ($dur->y) $S[] = $dur->y . ' years';
    if ($dur->m) $S[] = $dur->m . ' months';
    if ($dur->d) $S[] = $dur->d . ' days';
    if ($dur->h) $S[] = $dur->h . ' h';
    if ($dur->i) $S[] = $dur->i . ' min';
    if ($dur->s) $S[] = $dur->s . ' sec';
    $text = implode(' ', $S);;
    if ($text == '') $text = 'now' ; else $text = $text . ' ago';
    return $text;
}

$db = new sql_db('');
$Z = $db->execute('SELECT * FROM `Main`.`Instruments`');
$tz = new DateTimeZone('UTC');
for($i=0;$i<count($Z);$i++) {
    $instrument_id = $Z[$i]['id'];
    $W = $db->execute(sprintf('SELECT `date`,`close` FROM `Main`.`CandlesKalman` WHERE instrument_id=%d ORDER BY `date` DESC LIMIT 1', $instrument_id));
    if (count($W)) {
        $last_date = date_create_from_format("Y-m-d H:i:s", $W[0]['date'], $tz);
        $date_now  = date_create_from_format("Y-m-d H:i:s", gmdate("Y-m-d H:i:s"), $tz);
        $dur = $last_date->diff($date_now);
        $dur_string = DurationToString($dur);
        $Z[$i]['age'] = $dur_string;
        $Z[$i]['close'] = $W[0]['close'];
    } else {
        $Z[$i]['age'] = 'no data';
        $Z[$i]['close'] = 0.0;
    }
}

$s = '';
foreach($Z as $I) {
    $disabled = $I['disabled'];
    $row_class = ($disabled ==0 ? 'active_asset_row' : 'disabled_asset_row');
    $onclick = sprintf('show_graph(%d, \'%s\');', $I['id'],$I['description']);
    $s .= sprintf('<tr style="height:40px" class="%s">', $row_class);
    $s .= sprintf('<td>%d</td>', $I['id']);
    $s .= sprintf('<td>%s</td>', $I['name']);
    $s .= sprintf('<td>%s</td>', $I['description']);
    $s .= sprintf('<td>%s</td>', $I['age']);
    $s .= sprintf('<td>%.2f</td>', $I['close']);
    $s .= sprintf('<td title="Show data"><img src="/img/Symb_magnifier.png" name="symbol_magnifier" class="symbol_magnifier" onclick="%s"/></td>',$onclick);
    $s .= '</tr>';
}
print $s;
?>
</table>
</div>


<div>
<input type="submit" id="button_asset_showhide" value="Hide disabled" onclick="toggle_disabled()" />
</div>
<script>
function toggle_disabled() { 
    $(".disabled_asset_row").toggle(); 
    if ( $("#button_asset_showhide").val() == "Hide disabled" ) {
        $("#button_asset_showhide").val("Show disabled");
    } else {
        $("#button_asset_showhide").val("Hide disabled");
    }
}
</script>


</div>

<script>


function CreateGraphDialog()
{
    $("#graphwindow").dialog({
        width:800,height:500,title:"Artificial Data",modal:true,
        appendTo:"#sub_frame",
        autoOpen: false,
        close: function (event, ui) { DestroyChart(); }
    });

}


function CreateDialogs() {
}

function DestroyChart()
{
    //if($('#candlechart').highcharts() != undefined)
        //$('#candlechart').highcharts().destroy();
    //$("#graphwindow").dialog('destroy');

}


function show_graph(id, name)
{
    CreateGraphDialog();
    var seriesOption = [];
    $.getJSON("/scripts/show_linegraph.php?callback=?&table=CandlesKalman&column=close&id="+id+"&limit=5000", function(data)
    {
        //console.log(data);
        seriesOption[0] ={
            name: "Artificial",
            data: data,
        };
        $.getJSON("/scripts/show_linegraph.php?callback=?&table=Candles&column=close&id="+id+"&limit=5000", function(data1)
        {
            seriesOption[1] = {
                name: "Real",
                data: data1,
                lineWidth : 0,
                marker : {
                    enabled : true,
                    radius : 2,
                },      
            };
            console.log(seriesOption);
            var colors = ["#7cb5ec", "#970C0C"];
            Highcharts.getOptions().colors = colors;
            var displaychart = new Highcharts.StockChart({
                chart: {
                    renderTo: linechart,
                },
                title : {
                    text : name,
                },
                xAxis: {
                    ordinal: false,
                },
                exporting:  {
                    enabled: true,
                },
                tooltip: {
                    pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>',
                    valueDecimals: 2
                },
                series : seriesOption
            });
        });
    });
    $("#graphwindow").dialog('open');
}


</script>


<div id="graphwindow" style="display:none">
    <div id="linechart" style="height:400px">
    </div>
</div>

</body>
</html>
