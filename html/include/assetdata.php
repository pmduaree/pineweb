<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="/jquery/jquery-ui-1.10.4.css">
<link rel="stylesheet" type="text/css" href="/css/oleum.css">
<script src="/jquery/jquery-1.10.2.js"></script>
<script src="/jquery/jquery-ui-1.10.4.js"></script>
<script type="text/javascript" src="http://code.highcharts.com/stock/highstock.js"></script>
<script type="text/javascript" src="http://code.highcharts.com/stock/modules/exporting.js"></script>
<script>function RefreshParent() { $("#sub_frame").load("/include/assetdata.php"); } </script>
</head>

<body>


<div id="current_assets">

<style>
    .disabled_asset_row { color: #999999 ; }
</style>
<?php
require_once('sql_db.php');
?>


<div id="asset_table_container">
<h2> Candles </h2>
<table>
<tr>
<th>id</th>
<th style="width:70px">name</th>
<!-- <th style="width:150px">ticker</th> -->
<th style="width:390px">description</th>
<!-- <th>open market</th> -->
<!-- <th>trading day</th> -->
<th style="width:140px">last data</th>
<th style="width:80px">last price</th>
<th></th>
<th></th>
<th title="Add a new asset"><img src="/img/Symb_add.png" name="symbol_add" class="symbol_add" onclick="onAddAsset();" /></th>
</tr>
<?php

function DurationToString($dur)
{
    $tz = new DateTimeZone('UTC');
    $td1 = new DateTime("now", $tz);
    $td2 = new DateTime("now", $tz);
    //print_r($td1);
    $td1 = $td1->sub($dur);
    $td2 = $td2->sub(new DateInterval('PT66S'));
    //print_r($td1);
    //print_r($td2);
    //die();
    if ($td1 > $td2) return 'just now';
    $S = array();
    if ($dur->y) $S[] = $dur->y . ' years';
    if ($dur->m) $S[] = $dur->m . ' months';
    if ($dur->d) $S[] = $dur->d . ' days';
    if ($dur->h) $S[] = $dur->h . ' h';
    if ($dur->i) $S[] = $dur->i . ' min';
    if ($dur->s) $S[] = $dur->s . ' sec';
    $text = implode(' ', $S);;
    if ($text == '') $text = 'now' ; else $text = $text . ' ago';
    return $text;
}

$db = new sql_db('');
$Z = $db->execute('SELECT * FROM `Main`.`Instruments` WHERE `disabled` = 0');
$tz = new DateTimeZone('UTC');
for($i=0;$i<count($Z);$i++) {
    $instrument_id = $Z[$i]['id'];
    $W = $db->execute(sprintf('SELECT `date`,`close` FROM `Main`.`Candles` WHERE instrument_id=%d ORDER BY `date` DESC LIMIT 1', $instrument_id));
    if (count($W)) {
        $last_date = date_create_from_format("Y-m-d H:i:s", $W[0]['date'], $tz);
        $date_now  = date_create_from_format("Y-m-d H:i:s", gmdate("Y-m-d H:i:s"), $tz);
        $dur = $last_date->diff($date_now);
        $dur_string = DurationToString($dur);
        $Z[$i]['last_date'] = $W[0]['date'];
        $Z[$i]['age'] = $dur_string;
        //$Z[$i]['last_date'] = gmdate("Y-m-d H:i:s");
        $Z[$i]['last_price'] = $W[0]['close'];
    } else {
        $Z[$i]['last_date'] = 'no data';
        $Z[$i]['age'] = 'no data';
        $Z[$i]['last_price'] = 0.0;
    }
}

$s = '';
foreach($Z as $I) {
    $disabled = $I['disabled'];
    $row_class = ($disabled ==0 ? 'active_asset_row' : 'disabled_asset_row');
    $onclick = sprintf('show_graph(%d, \'%s\');', $I['id'],$I['description']);
    $s .= sprintf('<tr class="%s">', $row_class);
    $s .= sprintf('<td>%d</td>', $I['id']);
    $s .= sprintf('<td>%s</td>', $I['name']);
    //$s .= sprintf('<td>%s</td>', $I['ticker']);
    $s .= sprintf('<td>%s</td>', $I['description']);
    //$s .= sprintf('<td>%s</td>', $I['open_market']);
    //$s .= sprintf('<td>%s</td>', $I['trading_day']);
    $s .= sprintf('<td>%s</td>', $I['age']);
    //$s .= sprintf('<td>%s</td>', $I['last_date']);
    $s .= sprintf('<td>%.3f</td>', $I['last_price']);
    $s .= sprintf('<td title="Show data"><img src="/img/Symb_magnifier.png" name="symbol_magnifier" class="symbol_magnifier" onclick="%s"/></td>',$onclick);
    $s .= sprintf('<td title="Edit asset"><img src="/img/Symb_edit.png" name="symbol_edit" class="symbol_edit" onclick="onEditAsset(%d,\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',%d,%.4f)" /></td>',
                   $I['id'],$I['name'],$I['ticker'],$I['description'],$I['source'],$I['open_market'],$I['trading_day'],$I['commission'],$I['lotsize'],$I['margin']);
    $s .= sprintf('<td title="Remove asset"><img src="/img/Symb_delete.png" name="symbol_delete" class="symbol_delete" onclick="onRemoveAsset(%d)" /></td>', $I['id']);
    $s .= '</tr>';
}
print $s;
?>
</table>
</div>


<div>
<input type="submit" id="button_asset_showhide" value="Hide disabled" onclick="toggle_disabled()" />
</div>
<script>
function toggle_disabled() { 
    $(".disabled_asset_row").toggle(); 
    if ( $("#button_asset_showhide").val() == "Hide disabled" ) {
        $("#button_asset_showhide").val("Show disabled");
    } else {
        $("#button_asset_showhide").val("Hide disabled");
    }
}
</script>


</div>

<script>



function CreateAddAssetDialog(desc)
{
    var windowtitle = desc + ' Asset';
    // initialize the dialog
    $("#newassetwindow").dialog({
        width:800,height:500,
        title : windowtitle,
        modal : true,
        appendTo : "#sub_frame",
        autoOpen : false,
        close: function(event, ui) { DestroyDialog(); }
    });

}


function CreateGraphDialog()
{
    $("#graphwindow").dialog({
        width:800,height:500,title:"Candle chart",modal:true,
        appendTo:"#sub_frame",
        autoOpen: false,
        close: function (event, ui) { DestroyChart(); }
    });

}


function CreateDialogs() {
}

function DestroyChart()
{
    //if($('#candlechart').highcharts() != undefined)
        //$('#candlechart').highcharts().destroy();
    $("#graphwindow").dialog('destroy');

}


function DestroyDialog() {
    $("#newassetwindow").dialog('destroy');

}

/*$(function() {
    CreateDialogs();
});*/



function onAddAsset() { 
    CreateAddAssetDialog('Add');
    $("#addnewasset_id").val("0");
    $("#addnewasset_id").val("");
    $("#addnewasset_name").val("");
    $("#addnewasset_cqgticker").val("");
    $("#addnewasset_desc").val("");
    $("#addnewasset_openmarket").val("Mon@0->Mon@22,Tue@0->Tue@22,Wed@0->Wed@22,Thu@0->Thu@22,Fri@0->Fri@22");
    $("#addnewasset_tradingday").val("Mon@11->Mon@19,Tue@11->Tue@19,Wed@11->Wed@19,Thu@11->Thu@19,Fri@11->Fri@19");
    $("#newassetwindow").dialog("option", "buttons", [ {text: "Add Asset", click: function() { onNewAssetSubmit(); $(this).dialog("close"); } } ] );
    $("#newassetwindow").dialog('open');
}

function onEditAsset(assetid,assetname,cqgticker,desc,source,openmarket,tradingday,commission,lotsize,margin)
{
    // make the dialog box.
    CreateAddAssetDialog('Update');
    $("#addnewasset_id").val(assetid);
    $("#addnewasset_name").val(assetname);
    $("#addnewasset_cqgticker").val(cqgticker);
    $("#addnewasset_desc").val(desc);
    $("#addnewasset_source").val(source);
    $("#addnewasset_openmarket").val(openmarket);
    $("#addnewasset_tradingday").val(tradingday);
    $("#addnewasset_commission").val(commission);
    $("#addnewasset_lotsize").val(lotsize);
    $("#addnewasset_margin").val(margin);
    $("#newassetwindow").dialog("option", "buttons", [ { text: "Update Asset", click: function() { onNewAssetSubmit(); $(this).dialog("close"); } } ] );
    $("#newassetwindow").dialog('open');
}

function onRemoveAsset(assetid) {
    var didConfirm = confirm("Are you sure you want to remove the asset " + assetid + "? This will also remove all associated signals.");
    if (didConfirm == true) {
        var link="/scripts/remove_asset.php?id=" + assetid;
        $.get(link).done( function(data) { 
            if (data.length > 10) alert(data); 
            RefreshParent();
        });
    }
}

function show_graph(id, name)
{
    CreateGraphDialog();
    $.getJSON("/scripts/show_candlechart.php?callback=?&id="+id+"&name="+name, function(data)
    {
        //console.log(data);
        var displaychart = new Highcharts.StockChart({
            chart: {
                renderTo: 'candlechart',
            },
            title : {
                text : name,
            },

            series : [{
                type : 'candlestick',
                name : name, 
                data : data,
            }]

        });

    });
    $("#graphwindow").dialog('open');
}


</script>

<div id="newassetwindow" style="display:none">

    <input type="hidden" name="addnewasset_id" id="addnewasset_id" value="0" />
    <div class="add_fld">
    <label for="addnewasset_name">Name</label>
    <input type="text" name="addnewasset_name" id="addnewasset_name" value="" />
    </div>
    <div class="add_fld">
    <label for="addnewasset_cqgticker">Bloomberg Ticker</label>
    <input type="text" name="addnewasset_cqgticker" id="addnewasset_cqgticker" value="" />
    </div>
    <div class="add_fld">
    <label for="addnewasset_desc">Description</label>
    <input type="text" name="addnewasset_desc" style="width: 300px;" id="addnewasset_desc" value="" />
    </div>
    <div class="add_fld">
    <label for="addnewasset_source">Source</label>
    <input type="text" name="addnewasset_source" style="width: 70px;" id="addnewasset_source" value="Bloomberg" readonly />
    </div>
    <br />
    <div class="add_fld">
    <label for="addnewasset_openmarket">Market Opening Times</label>
    <input type="text" name="addnewasset_openmarket" id="addnewasset_openmarket" style="width: 700px;" value="" />
    </div>
    <br />
    <div class="add_fld">
    <label for="addnewasset_tradingday">Trading Day</label>
    <input type="text" name="addnewasset_tradingday" id="addnewasset_tradingday" style="width: 700px;" value="" />
    </div>
    <br />
    <div class="add_fld">
    <label for="addnewasset_commission">Commission</label>
    <input type="text" name="addnewasset_commission" style="width: 200px;" id="addnewasset_commission" value="percent=0.25,maxusd=5.0" />
    </div>
    <div class="add_fld">
    <label for="addnewasset_lotsize">Lot Size</label>
    <input type="number" name="addnewasset_lotsize" style="width: 100px;" id="addnewasset_lotsize" value="1000" />
    </div>
    <div class="add_fld">
    <label for="addnewasset_margin">Margin</label>
    <input type="number" name="addnewasset_margin" style="width: 100px;" id="addnewasset_margin" value="6000.00" />
    </div>
</div>  <!-- newassetwindow -->

<div id="graphwindow" style="display:none">
    <div id="candlechart" style="height:400px">
    </div>
</div>

<script>
function onNewAssetSubmit() {
    var link = "/scripts/add_instrument.php";
    link = link + "?id=" + $("#addnewasset_id").val();
    link = link + "&instrument_name=" + $("#addnewasset_name").val();
    link = link + "&ticker=" + $("#addnewasset_cqgticker").val();
    link = link + "&instrument_desc=" + $("#addnewasset_desc").val();
    link = link + "&source=" + $("#addnewasset_source").val();
    link = link + "&open_market=" + $("#addnewasset_openmarket").val();
    link = link + "&trading_day=" + $("#addnewasset_tradingday").val();
    link = link + "&commission=" + $("#addnewasset_commission").val();
    link = link + "&lotsize=" + $("#addnewasset_lotsize").val();
    link = link + "&margin=" + $("#addnewasset_margin").val();
    //alert(link);
    $.get(link).done( function(data) { 
        if (data.length > 10) alert(data); 
        RefreshParent();
        });
}
</script>


</body>
</html>
