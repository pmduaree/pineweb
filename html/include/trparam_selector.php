<?php
require_once('sql_db.php');

class TrparamSelector
{
    function __construct($myid='trparam_id')
    {
        $this->id = $myid;

        $db = new sql_db('Main');
        $query="SELECT * FROM  `Main`.`TraderParameters`";
        $Z = array();
        if ($result = $db->mysqli->query($query)) {
            while($row = $result->fetch_assoc()) {
                $Z[] = $row;
            }
            $result->close();
        }
        $this->parameters = $Z;
    }

    public function __toString()
    {
        $s = '';
        $s .= sprintf('<select style="width: 800px; " name="%s" id="%s">', $this->id, $this->id);
        foreach($this->parameters as $I) {
            $label = sprintf('%d: %s', $I['id'], $I['param']);
            $s .= sprintf('<option value="%d">%s</option>', $I['id'], $label);
        }
        $s .= '</select>';
        return $s;
    }
}

?>

