<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="/jquery/jquery-ui-1.10.4.css">
<link rel="stylesheet" type="text/css" href="/css/oleum.css">
<script src="/jquery/jquery-1.10.2.js"></script>
<script src="/jquery/jquery-ui-1.10.4.js"></script>
</head>

<?php
require_once('sql_db.php');
$db = new sql_db('');
?>
<body>

<div class="subparent" id="strategyportfoliosubparent">
<h2>Strategy Collection</h2>

<div>
<table>
<tr>
<th width="70px">Strategy</th>
<th width="70px">Indicator</th>
<th width="400px">Param</th>
<th width="40px">Period</th>
<th width="300px">Assets</th>
<th width="30px"></th>
<th width="30px"><img src="/img/Symb_add.png" name="symbol_add" class="symbol_add" id="addnew_strategy" /></th>
</tr>
<?php
$query = "SELECT S.id AS ids_, T.Name AS Name, E.CurrentParam AS CurrentParam, S.freq AS freq,
     S.disabled AS disabled, S.portfolio_id AS portfolio_id 
     FROM Main.Strategies AS S
     JOIN Main.SignalerEngines AS E 
     ON S.SignalerEngine_id = E.id 
     JOIN Main.SignalerType AS T 
     ON E.Type_id = T.id";
$strategies = $db->executeQuery($query);
$s = '';
foreach($strategies as $X)
{
    //print_r($X);
    $portfolioid = $X['ids_'];
    $row_id = sprintf('portfoliorow_%d', $portfolioid);
    $s .= sprintf('<tr class="portfolio_row" id="%s">', $row_id);
    $s .= sprintf('<td>%d</td>', $portfolioid);
    $s .= sprintf('<td>%s</td>', $X['Name']);
    $s .= sprintf('<td>%s</td>', $X['CurrentParam']);
    $s .= sprintf('<td>%d</td>', $X['freq']);
    $s .= sprintf('<td><ul>');
    $query = 'SELECT name FROM Main.Instruments AS I JOIN Main.Portfolio AS P ON I.id = P.instrument_id AND P.portfolio_id = \''. $X['portfolio_id'].'\';';
    $XX = $db->executeQuery($query);
    foreach($XX as $I)
        $s .= sprintf('<li>%s</li>', $I['name']);
    $s .= sprintf("</ul></td>");
    $name = $X['disabled'] ? 'Symb_red.png' : 'Symb_green.png';
    $s .= sprintf('<td><img width="30" height="30" onClick="DisableStrategy(%d,%d)" src="img/%s"> </td>',$portfolioid,$X['disabled'],$name);
    $s .= sprintf('<td><img class="symbol_delete" name="symbol_delete" src="/img/Symb_delete.png" onclick="DeleteStrategyPortfolio(%d)" /></td>', $portfolioid);
    $s .= sprintf("</tr>");
}
print $s;
?>
</table>
<script>
function toggle_traderpar_row(rowname) { 
    var z = '#' + rowname ; $(z).toggle(); 
}

function DisableStrategy(id, disabled)
{
    link = "/scripts/disable_strategy.php?id="+id
        +"&disabled="+(disabled ^ 1);
    //console.log(link);
    $.get(link).done( function(data) { 
        if (data.length > 10) alert(data); 
        RefreshParent();
    });
}

function DeleteStrategyPortfolio(portfolioid) { 
    var didconfirm = confirm("Are you sure you want to remove the strategy? This will also delete all its signals.");
    if (didconfirm == true) {
        var link="/scripts/remove_strategyportfolio.php?id=" + portfolioid;
        $.get(link).done( function(data) { 
            if (data.length > 10) alert(data); 
            RefreshParent();
        });
    }
}
</script>
<style type="text/css">
    .subtrader_row  td { background-color: #dddddd; }
    td:empty { border: none; background: none; }
</style>

</div>

<h2>Strategy Optimization</h2>


<div>
<table>
<tr>
<th width="30px">Strategy</th>
<th width="180px">Date from</th>
<th width="180px">Date to</th>
<th width="80px">Money</th>
<th width="160px">Status</th>
<th width="30px"><img src="/img/Symb_add.png" name="symbol_add" class="symbol_add" id="addnew_optimization" /> </th>
</tr>
<?php
$query = "
    SELECT O.id, O.Strategy_id, O.date_from, O.date_to, O.Money, O.Status, P.percentage
    FROM `Main`.`OptimizationOrders` AS O
    JOIN (
        SELECT OptOrder_id, 
            (SUM( CASE WHEN Status = 2 THEN 1 ELSE 0 END)/COUNT(*) * 100) as percentage
            FROM Main.OptimizationParamTuples
            GROUP BY OptOrder_id
    ) AS P
    WHERE P.OptOrder_id = O.id
    ORDER BY O.id";
$Z = $db->executeQuery($query);
foreach($Z as $itm) {
    $s = '<tr>';
    $s .= sprintf('<td width="30px">%s</td>', $itm['Strategy_id']);
    $s .= sprintf('<td width="180px">%s</td>', $itm['date_from']);
    $s .= sprintf('<td width="180px">%s</td>', $itm['date_to']);
    $s .= sprintf('<td width="80px">%s</td>', $itm['Money']);
    $status = "";
    $s .= '<td width="160px">';
    switch($itm['Status'])
    {
        case 0: //Pending
            $s .= sprintf('%s</td>', "Pending");
            break;
        case 1: //Running
            $s .= sprintf('%s</td>', "Running");
            $s .= sprintf('<td>%.2f%% completed</td>', $itm['percentage']);
            //print $itm['percentage'];

            break;
        case 2: //Finished
            $s .= sprintf('%s</td>', "Finished");
            $onclick = sprintf('showBestParameters(%s)', $itm['id']);
            $s .= sprintf('<td title="Show best parameters"><img src="/img/Symb_magnifier.png" name="symbol_magnifier" class="symbol_magnifier" onclick="%s"/></td>',$onclick);
            break;
        case 3: //Selected
            $s .= sprintf('%s</td>', "Parameters selected");
            $onclick = sprintf('showBestParameters(%s)', $itm['id']);
            $s .= sprintf('<td title="Show best parameters"><img src="/img/Symb_magnifier.png" name="symbol_magnifier" class="symbol_magnifier" onclick="%s"/></td>',$onclick);

            break;

    }
    $s .= '</tr>';
    $s .= "\n";
    print $s;
}
?>
</table>
</div>
<script>
function CancelOptTask(id) {
    var link = "/scripts/delete_opt_task.php?id=" + id;
    $.get(link).done(
        function(data) {
            if (data.length > 1) alert(data); 
            RefreshParent(); 
        } 
    );
}
</script>


<div class="oleum_popup" id="addnew_strategy_dialog" style="display:none;">
<div name="addnew_subframe" id="addnew_subframe">
    <div class="add_fld">
        <label for="addnew_indicator">Indicator</label>
        <!-- <input name="addnew_indicator" id="addnew_indicator" value="" /> -->
        <select name="addnew_indicator" id="addnew_indicator" value="">
        <?php
            $II = $db->executeQuery("SELECT id, Name FROM Main.SignalerType");
            foreach($II as $I) {
                printf('<option value="%d">%s</option>', $I['id'], $I['Name']);
            }
        ?>
        </select>
    </div>
    <div class="add_fld">
        <label for="addnew_freq">Period (min)</label>
        <input type="number" min="5" max="60" step="5" name="addnew_freq" id="addnew_freq" value="30" />
    </div>
    <div id="extra_fields">
    </div>
</div>  <!-- addnew_subframe -->
</div>  <!-- oleum_popup -->



<div class="oleum_popup" id="addnew_optimization_dialog" style="display:none;">
<div name="addnew_subframe" id="addnew_subframe">
    <div class="add_fld">
    <label for="addnew_opt">Strategy</label>
        <select id="addnew_opt" name="addnew_opt">
        <?php
        $II = $db->executeQuery("SELECT id FROM `Main`.`Strategies`");
        foreach($II as $I) {
            printf('<option value="%d">%s</option>', $I['id'], $I['id']);
        }
        ?>
        </select>
    </div>
    <div class="add_fld">
        <label for="addnew_date1">From date</label>
        <input name="addnew_date1" id="addnew_date1" value="" />
    </div>
    <div class="add_fld">
        <label for="addnew_date2">To date</label>
        <input name="addnew_date2" id="addnew_date2" value="" />
    </div>
    <div class="add_fld">
        <label for="addnew_numtrials">Money</label>
        <input title="Amount of money to invest during the order" type="number" min="1000" value="50000" max="100000" name="addnew_money" id="addnew_money" />
    </div>
    <div class="add_fld">
        <label for="addnew_resolution ">Resolution</label>
        <input title="The bigger, the slower but better parameters" type="number" min="1" value="5" max="10" name="addnew_resolution" id="addnew_resolution" />  
    </div>

</div>  <!-- addnew_subframe -->
</div>  <!-- oleum_popup -->

<div class="oleum_popup" id="params_dialog" style="display:none;">
<div name="opt_fields" id="opt_fields">
</div>  <!-- addnew_subframe -->
</div>  <!-- oleum_popup -->


<script>

function showBestParameters(opt_id)
{
    var link = "../scripts/get_best_parameters.php?id="+opt_id;
    $.get(link).done(
        function(data){
            CreateDialogParams();

            $("#opt_fields").html(data);
        }
    );
}

function selectedParams(strategyid,params)
{
    var link = "../scripts/set_best_parameters.php?id="+strategyid+"params="+params;
    $.get(link).done(
        function(data){
            RefreshParent();
        }
    );
}

function onNewStrategySubmit(){
    var link = "../scripts/add_new_strategy.php";
    var param = "";
    var aux = document.forms[0].param;
    if(aux != undefined)
    {
        for(var i = 0; i < aux.length; i++)
            param += aux[i].id + "=" + aux[i].value + ",";
        param = param.substr(0, param.length -1);
    }
    link += "?indicator="+$("#addnew_indicator").val();
    link += "&param="+param;
    link += "&freq="+$("#addnew_freq").val();
    link += "&port="+$("#addnew_portfolio").val();
    //console.log(link);
    $.get(link).done(
        function(data) {
            if (data.length > 10) alert(data); 
            RefreshParent();
        }
    );

}

function onChangeAsset()
{
    var id = $("#addnew_indicator").val();
    $.get("scripts/show_strategy_params.php?id="+id ,function(data)
    {
        //console.log(data);
        $("#extra_fields").html(data);
    });
}

function onNewParametersSubmit() {
    var strategyid = $("#addnew_opt").val();
    var date1 = $("#addnew_date1").val();
    var date2 = $("#addnew_date2").val();
    var resolution = $("#addnew_resolution").val();
    var money = $("#addnew_money").val();
    var link = "../scripts/add_opt_task.php";
    // link += "?strategyid="+strategyid;
    // link += "&date1='"+date1+"'";
    // link += "&date2='"+date2+"'";
    // link += "&resolution="+resolution;
    // link += "&money="+money;
    console.log(link);
    $.ajax(
    {
        type: "GET",
        url: link,
        data: {
            strategyid: strategyid,
            date1: date1,
            date2: date2,
            resolution: resolution,
            money : money,
        }
    }).done(function(data)
        {   
            //alert(data);
            RefreshParent();
        }
    );
    // $.get(link).done(
    //     function(data) {
    //         if (data.length > 10) alert(data); 
    //         RefreshParent();
    //     }
    // );
    RefreshParent();

}

function DestroyDialog()
{
    $("#addnew_date1").datepicker('destroy');
    $("#addnew_date2").datepicker('destroy');
    $("#addnew_optimization_dialog").dialog('destroy');
    $(".ui-datepicker").remove();
}

function CreateDialogStrategy()
{
    $("#addnew_strategy_dialog").dialog({
        appendTo : "#sub_frame",
        bgiframe: true,
        autoOpen: false,
        width:800,height:500,
        title:"Add a new strategy",
        buttons: [ { text: "Submit task", click: function() { onNewStrategySubmit(); $(this).dialog("close"); } } ],
        modal:true,
        close: function( event, ui) { $("#addnew_strategy_dialog").dialog('destroy'); }

    });    
    onChangeAsset();
}

function CreateDialogParams()
{
    $("#params_dialog").dialog({
        appendTo : "#sub_frame",
        bgiframe: true,
        autoOpen: false,
        width:800,height:600,
        title:"Select best parameters",
        //buttons: [ { text: "Submit order", click: function() { selectedParams(0); $(this).dialog("close"); } } ],
        modal:true,
        close: function( event, ui) { $("#params_dialog").dialog('destroy'); }

    });
    $("#params_dialog").dialog("open");

}


function CreateDialogOptimization()
{
    $("#addnew_optimization_dialog").dialog({
        appendTo : "#sub_frame",
        bgiframe: true,
        autoOpen: false,
        width:800,height:300,
        title:"Add a new optimization order",
        buttons: [ { text: "Submit order", click: function() { onNewParametersSubmit(); $(this).dialog("close"); } } ],
        modal:true,
        close: function( event, ui) { DestroyDialog(); }

    });
    
    $("#addnew_date1").datepicker({
        maxDate: "-4d",
        minDate: "-3m",
        defaultDate: "-2w -2d",
        firstDay: 1,
        dateFormat: 'yy-mm-dd'

    });
    $("#addnew_date2").datepicker({
        maxDate: "-1d",
        minDate: "-3m",
        defaultDate: "-2d",
        firstDay: 1,
        dateFormat: 'yy-mm-dd'
    });
}

$(function() {
    $("#addnew_optimization").click(function() { 
        CreateDialogOptimization();
        $("#addnew_optimization_dialog").dialog("open");
    });
    $("#addnew_strategy").click(function() { 
        CreateDialogStrategy();
        $("#addnew_strategy_dialog").dialog("open");
    });
    $("#addnew_indicator").change(function(){
        onChangeAsset();
    });
});

function RefreshParent() { $("#sub_frame").load("/include/oleum_strategies.php"); } 
</script>
</div>
</body>
</html>
