<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="/jquery/jquery-ui-1.10.4.css">
<link rel="stylesheet" type="text/css" href="/css/oleum.css">
<script src="/jquery/jquery-1.10.2.js"></script>
<script src="/jquery/jquery-ui-1.10.4.js"></script>
</head>

<body>

<h2>Indicator Types</h2>

<?php
require_once('sql_db.php');
?>

<div>
<?php
$db = new sql_db('');
$query = 'SELECT * FROM `Main`.`StrategyTypes`';
$Z = $db->executeQuery($query);
print '<table>';
print '<tr>';
print '<th style="width: 120px;">Indicator</th>';
print '<th style="width: 400px;">Description</th>';
print '<th style="width: 420px;">Parameters</th>';
print '<th style="width: 30px;"><img src="/img/Symb_add.png" class="symbol_add" name="symbol_add" id="button_new_indicatortype" /></th>';
print '</tr>';
foreach($Z as $zz) {
    print '<tr>';
    printf('<td>%s</td>', $zz['type']);
    printf('<td>%s</td>', $zz['info']);
    printf('<td>%s</td>', $zz['fields']);
    print '<td></td>';
    print '</tr>';
}
print '</table>';
?>
</div>

<div id="newindicatortype_container" style="display: none">
    <div id='newindicatortype_dialog' style='display:none'>
    <div class="add_fld">
        <label for="newindicatortype_type">Type</label><input id="newindicatortype_type" value="" />
    </div>
    <div class="add_fld">
        <label for="newindicatortype_desc">Description</label><input style="width: 400px" id="newindicatortype_desc" value="" />
    </div>
    <div class="add_fld">
        <label for="newindicatortype_params">Parameters</label><input style="width: 400px" id="newindicatortype_params" value="" />
    </div>

    </div>
</div>


<script>

function RefreshParent() { 
    $("#sub_frame").load("/include/oleum_indicatortypes.php"); 
} 


function AddNewIndicator(itype,idesc,iparams)
{
    $.ajax({ type: "POST",
             url: "/scripts/add_new_indicator_type.php", 
             data: { type : itype, desc : idesc, params : iparams },
             dataType: "html"
        }
    ).done( 
        function( html) {
                if (html.length > 0) alert(html);
                RefreshParent();
        }
    );
}




function DestroyNewIndicatorTypeDialog()
{
    $("#newindicatortype_dialog").dialog('destroy');
}

$("#button_new_indicatortype").click(function() {
    // open a new dialog.
    $("#newindicatortype_dialog").dialog({
        close: function(event,ui) { DestroyNewIndicatorTypeDialog()},
        title: 'Add a new indicator type',
        modal: true,
        width: 500,
        buttons: [ {text:'Add', click: function() { 
                  AddNewIndicator( $("#newindicatortype_type").val(), 
                                   $("#newindicatortype_desc").val(), 
                                   $("#newindicatortype_params").val() );
                  $(this).dialog('close');
                } } ]
    } );
    $(div).dialog('open');
    
});


</script>

</body>
</html>

