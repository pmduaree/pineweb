<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="/jquery/jquery-ui-1.10.4.css">
<link rel="stylesheet" type="text/css" href="/css/oleum.css">
<script src="/jquery/jquery-1.10.2.js"></script>
<script src="/jquery/jquery-ui-1.10.4.js"></script>

</head>

<body>


<h2>Indicator Signals</h2>

<?php
require_once('sql_db.php');
$db = new sql_db('');
?>


<div>

<!--<label for="is_asset">Asset</label>
<select id="is_asset" class="is_selector">
<?php
/*$query = 'SELECT * FROM `Main`.`Instruments`';
$Z = $db->executeQuery($query);
foreach($Z as $stt) {
    print sprintf('<option value="%d">%s</option>', $stt['id'], $stt['name']);
}
*/
?>
</select>-->

<label for="is_itype">Indicator Type</label>
<select id="is_itype" class="is_selector">
<?php
$query = 'SELECT * FROM `Main`.`StrategyTypes`';
$Z = $db->executeQuery($query);
foreach($Z as $stt) {
    print sprintf('<option value="%s">%s</option>', $stt['type'], $stt['type']);
}
?>
</select>

<label for="is_strategy">Strategy ID</label>
<select id="is_strategy" class="is_selector">
</select>

</div>

<div id="is_table">

</div>

<script>

function LoadStrategies()
{
    var itype = $("#is_itype").val();
    $.get("/scripts/load_indicators_of_type.php?itype=" + itype, function (data ) {
        var z = '<option value="0">All</option>' + data;
        $("#is_strategy").html(z);
    });
}

$("#is_itype").change( function() {
    LoadStrategies();
    $("#is_strategy").select(0);
});

function RefreshTable()
{
    var asset_id = $("#is_asset").val();
    var itype = $("#is_itype").val();
    var strategy = $("#is_strategy").val();
    var link = '/scripts/load_indicator_signal_table.php?asset='+asset_id+'&itype='+itype+'&strategy='+strategy;
    $("#is_table").load(link);
}

$(document).ready( function() {
    LoadStrategies();
    RefreshTable();
});

$(".is_selector").change(function() { 
    RefreshTable(); } );


</script>

</body>
</html>


