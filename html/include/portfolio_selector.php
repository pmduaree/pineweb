<?php
require_once('sql_db.php');

class PortfolioSelector
{
    function __construct($myid='portfolio_id')
    {
        $this->id = $myid;

        $db = new sql_db('Main');
        $query="SELECT * FROM  `Main`.`StrategyPortfolios`";
        $Z = array();
        if ($result = $db->mysqli->query($query)) {
            while($row = $result->fetch_assoc()) {
                $Z[] = $row;
            }
            $result->close();
        }
        $this->portfolios = $Z;
    }

    public function __toString()
    {
        $s = '';
        $s .= sprintf('<select name="%s" id="%s">', $this->id, $this->id);
        foreach($this->portfolios as $I) {
            //$label = sprintf('%d: %s', $I['id'], $I['note']);
            $label = sprintf('%d', $I['id']);
            $s .= sprintf('<option value="%d">%s</option>', $I['id'], $label);
        }
        $s .= '</select>';
        return $s;
    }
}

?>
