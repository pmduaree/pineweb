<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="/jquery/jquery-ui-1.10.4.css">
<link rel="stylesheet" type="text/css" href="/css/oleum.css">
<script src="/jquery/jquery-1.10.2.js"></script>
<script src="/jquery/jquery-ui-1.10.4.js"></script>
</head>

<body>
<?php
require_once('sql_db.php');
$db = new sql_db('');
?>

<h2>Trader Parameter Collection</h2>


<div id="trader_parameter_collection">
<table id="trparam_collection">
<?php
$ZZ = $db->executeQuery('SELECT * FROM `Main`.`TraderParameters`');
$all_keys = array();
for($i=0;$i<count($ZZ);$i++) {
    $xparam = explode(',', $ZZ[$i]['param']);
    $P = array();
    foreach($xparam as $kv) {
        $zz = explode('=', $kv);
        $k = $zz[0];
        $v = $zz[1];
        $all_keys[$k] = 1;
        $P[$k]=$v;
    }
    $ZZ[$i]['P'] = $P;
}
$keys = array_keys($all_keys);
$ZZ = $ZZ;
$s = '';
$s .= '<tr>';
$s .= '<th>ID</th>';
foreach($keys as $k) {
    $s .= sprintf('<th>%s</th>', $k);
}
$s .= '<th></th>';
$s .= '</tr>';
print $s;
foreach($ZZ as $Z){
    $s = '';
    $s .= '<tr>';
    $s .= sprintf('<td>%d</td>', $Z['id']);
    foreach($keys as $k) {
        $s .= sprintf('<td>%s</td>', $Z['P'][$k]);
    }
    $s .= sprintf('<td><img src="img/Symb_delete.png" class="symbol_delete" name="symbol_delete" onclick="DeleteTrparam(%d)" /></td>', $Z['id']);
    $s .= '</tr>';
    print $s;
}
?>
</table>
<script>
function DeleteTrparam(trparamid) { 
    var didCofirm = confirm("Are you sure you want to delete the trading parameter set?");
    if (didConfirm == true) {
        var link="/scripts/remove_trparams.php?id=" + trparamid;
        $.get(link).done( function(data) { 
            if (data.length > 10) alert(data); 
            RefreshParent();
        });
    }
}
</script>
</div> <!-- trader parameters collection -->


<h2>Parameter optimization</h2>

<div id="parameter_optimization_section">
<table>
<tr>
<th width="30px">Parameters</th>
<th width="30px">Asset</th>
<th width="30px">Strategy</th>
<th width="80px">DateFrom</th>
<th width="80px">DateTo</th>
<th width="150px">Note</th>
<th width="30px"><img src="/img/Symb_add.png" name="symbol_add" class="symbol_add" id="addnew_trparam_button" /></th>
</tr>
<?php
$ZZ = $db->executeQuery('SELECT * FROM `Main`.`TraderOptimization`');
for($i=0;$i<count($ZZ);$i++) {
    $s = '';
    $assetid = $ZZ[$i]['asset_id'];
    if ($assetid) {
        $query = sprintf("SELECT * FROM `Main`.`Instruments` WHERE `id`=%d", $assetid);
        $ZZ[$i]['asset'] = $query;
        $IX = $db->execute($query);
        $ZZ[$i]['asset'] = $IX[0]['name'];
    }
    $Z = $ZZ[$i];
    $s .= '<tr>';
    $s .= sprintf('<td width="30px">%d</td>', $Z['traderpar_id']);
    $s .= sprintf('<td width="30px">%s</td>', $Z['asset']);
    $s .= sprintf('<td width="30px">%s</td>', $Z['strategyportfolio_id']);
    $s .= sprintf('<td width="80px">%s</td>', $Z['date_from']);
    $s .= sprintf('<td width="80px">%s</td>', $Z['date_to']);
    $s .= sprintf('<td width="380px">%s</td>', $Z['note']);
    $s .= '</tr>';
    print $s;
}
?>
</table>
</div>



<div id="addnew_trparam_popup" style="display:none">
<div name="addnew_subframe" id="addnew_subframe">
    <div class="add_fld">
        <label for="addnew_assetid">Asset</label>
        <select id="addnew_assetid">
        </select>
    </div>
    <div class="add_fld">
        <label for="addnew_portfolioid">Strategy</label>
        <select id="addnew_portfolioid" name="addnew_portfolioid">
        </select>
    </div>
    <div class="add_fld">
        <label for="addnew_date1">From date</label>
        <input name="addnew_date1" id="addnew_date1" value="" />
    </div>
    <div class="add_fld">
        <label for="addnew_date2">To date</label>
        <input name="addnew_date2" id="addnew_date2" value="" />
    </div>
    <div class="add_fld">
        <label for="addnew_ntrials">Trials</label>
        <input name="addnew_ntrials" type="number" id="addnew_ntrials" value="10000" />
    </div>
</div> <!-- end of addnew_subframe -->

<script>


$("#addnew_trparam_popup").ready(function() {
    LoadAssets();
});

function LoadStrategies()
{
    var asset = $("#addnew_assetid").val();
    var link = "/scripts/load_strategies_for_asset.php?asset="+asset;
    //alert(link);
    $("#addnew_portfolioid").load("/scripts/load_strategies_for_asset.php?asset="+asset)
}

function LoadAssets()
{
    $("#addnew_assetid").load("/scripts/load_assets.php", function() {  
        $("#addnew_assetid").val( $("#addnew_assetid option:first").val() );
        LoadStrategies();
    });
}


$("#addnew_assetid").change(function() { LoadStrategies(); } );


function onNewTraderParametersSubmit() {
    var assetid = $("#addnew_assetid").val();
    var portfolioid = $("#addnew_portfolioid").val();
    var date1 = $("#addnew_date1").val();
    var date2 = $("#addnew_date2").val();
    var ntrials = $("#addnew_ntrials").val();
    var link = "/scripts/add_trp_opt_task.php?asset="+assetid+"&portfolio="+portfolioid+"&date1='"+date1+"'&date2='"+date2+"'"+"&ntrials="+ntrials;
    $.get(link).done(
        function(data) {
            if (data.length > 10) alert(data); 
        }
    );
    return true;
}
</script>


</div> <!-- addnew_trparam_popup -->

<script>

function DestroyDialog()
{
    $('#addnew_date1').datepicker('destroy');
    $('#addnew_date2').datepicker('destroy');
    $("#addnew_trparam_popup").dialog('destroy');
    $(".ui-datepicker").remove();
}


function CreateDialog() {


    $('#addnew_date1').datepicker({
        minDate: "-2m",
        maxDate: "-1w",
        defaultDate: "-2w 2d",
        dateFormat: "yy-mm-dd",
        firstDay: 1
    });
    $('#addnew_date2').datepicker({
        minDate: "-2m",
        maxDate: "-1d",
        defaultDate: "-2d",
        dateFormat: "yy-mm-dd",
        firstDay: 1

    });

    $("#addnew_trparam_popup").dialog({
        appendTo: "#sub_frame",
        autoOpen: false,
        width: 700,
        height: 300,
        title: 'New trader parameter set',
        modal: true,
        buttons: [ {text: 'Submit task', click : function() { 
            var retval = onNewTraderParametersSubmit(); 
            if (retval == true) {
                $(this).dialog('close'); 
            }
        } } ],
        modal:true,
        close: function( event, ui) { DestroyDialog(); }
    });
}


$(function() {
    $("#addnew_trparam_button").click( function() {
        CreateDialog();
        $("#addnew_trparam_popup").dialog("open");
    });
});
function RefreshParent() { $("#sub_frame").load("/include/oleum_trparams.php"); } 
</script>

</body>
</html>
