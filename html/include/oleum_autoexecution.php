<?php
print '<html>';
print '<head>';
print '</head>';

print '<body>';

print '<h2>Autoexecution Status</h2>';

require_once('sql_db.php');
$db = new sql_db('');
$query = 'SELECT I.id, O.id as trader_id ,O.TS_open, O.price_open, O.open_side, O.enter_id, O.asset_name
    FROM Trading.Orders AS O
    JOIN Main.Instruments AS I ON I.name = O.asset_name
    WHERE O.order_status = 1;
';
$Z = $db->executeQuery($query);

if (count($Z)) {
    print '<table style="boder:1px solid black">';
    print '<tr>';
    print '<th>Id</th>';   
    print '<th>Asset</th>';
    print '<th>Type</th>';
    print '<th>Quantity</th>';
    print '<th>Transaction Date</th>';
    print '<th>Open Price</th>';
    print '<th>Current Price</th>';
    print '<th>Current Profit</th>';

    print '</tr>';
    foreach($Z as $trade) {
        print '<tr>';
        print sprintf('<td>%d</td>', $trade['trader_id']);
        print sprintf('<td>%s</td>', $trade['asset_name']);
        print sprintf('<td>%s</td>', $trade['open_side']);
        print sprintf('<td>%d</td>', 1);
        print sprintf('<td>%s</td>', $trade['TS_open']);
        print sprintf('<td>%.3f</td>', $trade['price_open']);
        $query = sprintf("SELECT close FROM Main.Candles WHERE instrument_id = %s ORDER BY date DESC LIMIT 1", $trade['id']);
        $X = $db->executeQuery($query);
        $c = $X[0]['close'];
        print sprintf('<td>%.3f</td>', $c);
        $profit_color = 'yellow';
        $current_profit = 0;
        if($trade['open_side'] == 'Buy')
        {
            $profit_color = ($c < $trade['price_open']) ? 'red' : 'green';
            $current_profit = $c - $trade['price_open'];
        }
        else if ($trade['open_side'] == 'Sell')
        {
            $profit_color = ($c > $trade['price_open']) ? 'red' : 'green';
            $current_profit = $trade['price_open'] - $c;
        }
        print sprintf('<td style="background: %s" >%.3f</td>', $profit_color, $current_profit);
        print '</tr>';

    }
    print '</table>';
} else {
    print '<div>No active trades</div>';

}



print '</body>';
print '</html>';

?>


