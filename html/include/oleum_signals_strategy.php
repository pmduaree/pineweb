<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="/jquery/jquery-ui-1.10.4.css">
<link rel="stylesheet" type="text/css" href="/css/oleum.css">
<script src="/jquery/jquery-1.10.2.js"></script>
<script src="/jquery/jquery-ui-1.10.4.js"></script>
</head>

<body>

<h2>Strategy Signals</h2>

<?php
require_once('sql_db.php');
$db = new sql_db('');
?>


<div>

<!--
<label for="asset_id">Asset</label>
<select id="asset_id" class="ss_selector">
<?php
/*
$query = 'SELECT * FROM `Main`.`Instruments`';
$Z = $db->executeQuery($query);
foreach($Z as $stt) {
    print sprintf('<option value="%d">%s</option>', $stt['id'], $stt['name']);
}
*/
?>
</select>-->


<label for="portfolio_id">Portfolio</label>
<select id="portfolio_id" class="ss_selector">
<?php
$query = 'SELECT portfolio_id FROM `Main`.`Portfolio` WHERE portfolio_id > 0 GROUP BY portfolio_id';
$Z = $db->executeQuery($query);
foreach($Z as $stt) {
    print sprintf('<option value="%d">%d</option>', $stt['portfolio_id'], $stt['portfolio_id']);
}
?>
</select>

<label for="ss_howmany">Count</label>
<input type="number" value="25" step="5" max="200" min="10" id="ss_howmany" class="ss_selector" />


</div>


<div id="strategy_signal_table_contents">
</div>

<script>

function RefreshTable()
{
    var portfolio = $("#portfolio_id").val();
    var howmany = $("#ss_howmany").val();
    $("#strategy_signal_table_contents").load('/scripts/make_strategy_signal_table.php?strategy=' + portfolio + '&howmany='+howmany);
}



$(document).ready( function() {
    RefreshTable();
});

$(".ss_selector").change(function() { RefreshTable(); } );


</script>


</body>
</html>

