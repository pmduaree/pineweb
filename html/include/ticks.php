<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="/jquery/jquery-ui-1.10.4.css">
<link rel="stylesheet" type="text/css" href="/css/oleum.css">
<script src="/jquery/jquery-1.10.2.js"></script>
<script src="/jquery/jquery-ui-1.10.4.js"></script>
<script type="text/javascript" src="http://code.highcharts.com/stock/highstock.js"></script>
<script type="text/javascript" src="http://code.highcharts.com/stock/modules/exporting.js"></script>
<script>function RefreshParent() { $("#sub_frame").load("/include/ticks.php"); } </script>
</head>

<body>


<div id="current_assets">

<style>
    .disabled_asset_row { color: #999999 ; }
</style>

<h2>Bid/Ask </h2>
<div id="asset_table_container">
<table>
<tr>
<th>Id</th>
<th style="width:70px">Name</th>
<th style="width:370px">Description</th>
<th style="width:160px">Last Data</th>
<th>Best Bid</th>
<th>Best Ask</th>
<th>Midprice</th> 
<th></th>
</tr>
<?php

function DurationToString($dur)
{
    $tz = new DateTimeZone('UTC');
    $td1 = new DateTime("now", $tz);
    $td2 = new DateTime("now", $tz);
    //print_r($td1);
    $td1 = $td1->sub($dur);
    $td2 = $td2->sub(new DateInterval('PT66S'));
    //print_r($td1);
    //print_r($td2);
    //die();
    if ($td1 > $td2) return 'just now';
    $S = array();
    if ($dur->y) $S[] = $dur->y . ' years';
    if ($dur->m) $S[] = $dur->m . ' months';
    if ($dur->d) $S[] = $dur->d . ' days';
    if ($dur->h) $S[] = $dur->h . ' h';
    if ($dur->i) $S[] = $dur->i . ' min';
    if ($dur->s) $S[] = $dur->s . ' sec';
    $text = implode(' ', $S);;
    if ($text == '') $text = 'now' ; else $text = $text . ' ago';
    return $text;
}
require_once('sql_db.php');

$db = new sql_db('');
$Z = $db->execute('
    SELECT I.id, I.name, I.description, I.disabled, T.instrument_id, T.midprice, T.ask, T.bid, T.date FROM Main.Tick AS T
    INNER JOIN (
        SELECT instrument_id, MAX(date) as date FROM Main.Tick GROUP BY instrument_id
    ) AS A ON T.date = A.date AND T.instrument_id = A.instrument_id
    JOIN Main.Instruments AS I ON T.instrument_id = I.id
    ORDER BY I.id');
$tz = new DateTimeZone('UTC');
foreach($Z as $I)
{
    $instrument_id = $I['id'];
    $last_date = date_create_from_format("Y-m-d H:i:s", $I['date'], $tz);
    $date_now  = date_create_from_format("Y-m-d H:i:s", gmdate("Y-m-d H:i:s"), $tz);
    $dur = $last_date->diff($date_now);
    $dur_string = DurationToString($dur);
    $disabled = $I['disabled'];
    $row_class = ($disabled ==0 ? 'active_asset_row' : 'disabled_asset_row');
    $onclick = sprintf('show_graph(%d, \'%s\');', $I['id'],$I['description']);
    $s .= sprintf('<tr style="height:40px" class="%s">', $row_class);
    $s .= sprintf('<td>%d</td>', $I['id']);
    $s .= sprintf('<td>%s</td>', $I['name']);
    $s .= sprintf('<td>%s</td>', $I['description']);
    $s .= sprintf('<td>%s</td>', $dur_string);
    $s .= sprintf('<td>%.2f</td>', $I['bid']);
    $s .= sprintf('<td>%.2f</td>', $I['ask']);
    $s .= sprintf('<td>%.2f</td>', $I['midprice']);
    $s .= sprintf('<td title="Show data"><img src="/img/Symb_magnifier.png" name="symbol_magnifier" class="symbol_magnifier" onclick="%s"/></td>',$onclick);
    $s .= '</tr>';
}
print $s;
?>
</table>
</div>


<div>
<input type="submit" id="button_asset_showhide" value="Hide disabled" onclick="toggle_disabled()" />
</div>
<script>
function toggle_disabled() { 
    $(".disabled_asset_row").toggle(); 
    if ( $("#button_asset_showhide").val() == "Hide disabled" ) {
        $("#button_asset_showhide").val("Show disabled");
    } else {
        $("#button_asset_showhide").val("Hide disabled");
    }
}
</script>


</div>

<script>


function CreateGraphDialog()
{
    $("#graphwindow").dialog({
        width:800,height:500,title:"Bid/Ask",modal:true,
        appendTo:"#sub_frame",
        autoOpen: false,
        close: function (event, ui) { DestroyChart(); }
    });

}


function CreateDialogs() {
}

function DestroyChart()
{
    //if($('#candlechart').highcharts() != undefined)
        //$('#candlechart').highcharts().destroy();
    //$("#graphwindow").dialog('destroy');

}


function show_graph(id, name)
{
    CreateGraphDialog();
    $.getJSON("/scripts/show_linegraph.php?callback=?&table=Tick&column=midprice&id="+id+"&limit=10000", function(datamid)
    {
        //console.log(data);
    $.getJSON("/scripts/show_linegraph.php?callback=?&table=Candles&column=close&id="+id+"&limit=1000", function(dataclose) 
    {
    $.getJSON("/scripts/show_linegraph.php?callback=?&table=Tick&column=bid&id="+id+"&limit=10000", function(databid)
    {
    $.getJSON("/scripts/show_linegraph.php?callback=?&table=Tick&column=ask&id="+id+"&limit=10000", function(dataask)
    {
            var displaychart = new Highcharts.StockChart({
                chart: {
                    renderTo: linechart,
                },
                title: {
                    text: name
                },

                yAxis: [{
                    labels: {
                        align: 'right',
                        x: -3
                    },
                    title: {
                        text: 'Close'
                    },
                    height: '60%',
                    lineWidth: 2
                }, {
                    labels: {
                        align: 'right',
                        x: -3
                    },
                    title: {
                        text: 'Bid/Ask'
                    },
                    top: '50%',
                    height: '50%',
                    offset: 0,
                    lineWidth: 2
                }],
                series: [{
                    type: 'line',
                    name: 'Close',
                    data: dataclose,
                }, {
                    type: 'line',
                    name: 'Mid',
                    data: datamid,
                    yAxis: 1,
                    tooltip: {
                        valueDecimals: 2
                    },
                }, {
                    type: 'line',
                    name: 'Bid',
                    data: databid,
                    yAxis: 1,
                    tooltip: {
                        valueDecimals: 2
                    }
                },{
                    type: 'line',
                    name: 'Ask',
                    data: dataask,
                    yAxis: 1,
                    tooltip: {
                        valueDecimals: 2
                    },
                }]


            });


        // var displaychart = new Highcharts.StockChart({
        //     chart: {
        //         renderTo: linechart,
        //     },
        //     title : {
        //         text : name,
        //     },
        //     xAxis: {
        //         ordinal: false,
        //     },
        //     exporting:  {
        //         enabled: true,
        //     },
        //     series : [{
        //         name : "Mid",
        //         data : data,
        //         marker : {
        //             enabled : true,
        //             radius : 3,
        //         },
        //         shadow : true,
        //         tooltip : {
        //             valueDecimals : 2,
        //         },
        //     }]

        // });
    });
    });
    });
    });
    $("#graphwindow").dialog('open');
}


</script>


<div id="graphwindow" style="display:none">
    <div id="linechart" style="height:400px">
    </div>
</div>

</body>
</html>
