<?php


require_once('sql_db.php');

class sql_rawdata
{
    public $sqldb;
    public $dbname;
    function __construct($dbname='tix')
    {
        $this->dbname = $dbname;
        $this->sqldb = new sql_db($dbname);
    }

    function make_query($query)
    {
        $Z = array();
        if ($result = $this->sqldb->mysqli->query($query)) {
            while($row = $result->fetch_assoc()) {
                $Z[] = $row;
            }
            $result->close();
        } 
        return $Z;
    }

    function get_data($instrument_id, $fields, $limit, $freq=-1)
    {
        $sql_fields = implode(',', $fields);

        // prepare the query
        $condx=sprintf('instrument_id=%d', $instrument_id);
        if ($freq > 0) {
            $condx .= sprintf(' AND freq=%d', $freq);
        }
        $query=sprintf("SELECT %s FROM `Main`.`Candles` WHERE (%s) ORDER BY `date` DESC LIMIT %d", $sql_fields, $condx, $limit);
        # run the query
        $X = $this->make_query($query);
        $DATA = array();
        foreach($X as $row) {
            $Z = array();
            foreach($fields as $k)
                $Z[$k] = $row[$k];
            $DATA[] = $Z;
        }
        return $DATA;
    }

    function get_in_range($instrument_id, $freq, $date1, $date2)
    {
        $querx = 'SELECT * FROM `Main`.`Candles` WHERE (`instrument_id`=%d AND `freq`=%d AND `date`>=\'%s\' AND `date`<=\'%s\') ORDER BY `date` ASC';
        $query = sprintf($querx, $instrument_id, $freq, $date1, $date2);
        $Z = $this->make_query($query);
        return $Z;
    }

    function delete_for_instrument($instrument_id)
    {
        $query = sprintf('DELETE FROM `Main`.`Candles` WHERE `instrument_id`=%d', $instrument_id);
        $result = $this->sqldb->mysqli->query($query);
    }


    function get_last($instrument_id, $fields)
    {
        return $this->get_data($instrument_id, $fields, 1);
    }

}




?>
