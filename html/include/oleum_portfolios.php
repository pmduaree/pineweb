<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="/jquery/jquery-ui-1.10.4.css">
<link rel="stylesheet" type="text/css" href="/css/oleum.css">
<script src="/jquery/jquery-1.10.2.js"></script>
<script src="/jquery/jquery-ui-1.10.4.js"></script>
</head>

<?php
require_once('sql_db.php');
$db = new sql_db('');
?>

<body>

<div class="subparent" id="strategyportfoliosubparent">
<h2>Portfolios</h2>

<div>
<table>
<tr>
<th width="70px">Portfolio</th>
<th width="400px">Assets</th>
<th> <img src="/img/Symb_add.png" name="symbol_add" class="symbol_add" id="addnew_button" /> </th>
</tr>
<?php

$query = "SELECT P.portfolio_id, I.description FROM Main.Instruments AS I JOIN Main.Portfolio AS P ON I.id = P.instrument_id AND P.portfolio_id != 0 ORDER BY P.portfolio_id ASC";
$result = $db->executeQuery($query);
//print_r($result);
$ids = array();
$s = '';

foreach($result as $X)
    array_push($ids, $X['portfolio_id']);
$portfolioids = array_unique($ids);
//print_r($portfolioids);
foreach($portfolioids as $X)
{
    //print_r($X);
    $row_id = sprintf('portfoliorow_%d', $X);
    $s .= sprintf('<tr class="portfolio_row" id="%s">', $row_id);
    $s .= sprintf('<td>%d</td>', $X);
    $s .= sprintf('<td><ul>');

    foreach($result as $I)
        if($I['portfolio_id'] == $X)
            $s .= sprintf('<li>%s</li>', $I['description']);
    $s .= sprintf("</ul></td>");
    $s .= sprintf('<td><img class="symbol_delete" name="symbol_delete" src="/img/Symb_delete.png" onclick="DeletePortfolio(%d)" /></td>', $X);
    $s .= sprintf("</tr>");


}
print $s;
?>
</table>
<script>
function toggle_traderpar_row(rowname) { 
    var z = '#' + rowname ; $(z).toggle(); 
}

function DeletePortfolio(portfolioid) { 
    var didconfirm = confirm("Are you sure you want to remove the portfolio?");
    if (didconfirm == true) {
        var link="/scripts/delete_portfolio.php?id=" + portfolioid;
        $.get(link).done( function(data) { 
            if (data.length > 10) alert(data); 
            RefreshParent();
        });
    }
}
</script>
<style type="text/css">
    .subtrader_row  td { background-color: #dddddd; }
    td:empty { border: none; background: none; }
</style>

</div>

<div class="oleum_popup" id="addnew_portfolio_dialog" style="display:none;">
<div name="addnew_subframe" id="addnew_subframe">
    <div class="add_fld">
    <label for="addnew_assetid">Assets:</label>
        <form>
            <?php
            $II = $db->executeQuery("SELECT * FROM `Main`.`Instruments`");
            foreach($II as $I) {
                printf('<input type="checkbox" name="assets" value="%d">%s<br>', $I['id'], $I['name']);
            }
            ?>
        </form>
    </div>
</div>  <!-- addnew_subframe -->
</div>  <!-- oleum_popup -->

<script>
function onNewParametersSubmit() {
    var assets = document.forms[0].assets;
    var txt = "";
    var i;
    for (i = 0; i < assets.length; i++) 
        if (assets[i].checked) 
            txt = txt + assets[i].value + ",";
    var link = "../scripts/add_portfolio.php?assets="+txt.substring(0,txt.length-1);
    //console.log(link);
    $.get(link).done(
        function(data) {
            if (data.length > 10) alert(data); 
            RefreshParent();
        }
    );
}

function DestroyDialog()
{
    $("#addnew_strategy_dialog").dialog('destroy');
}

function CreateDialog()
{
    $("#addnew_portfolio_dialog").dialog({
        appendTo : "#sub_frame",
        bgiframe: true,
        autoOpen: false,
        width:800,height:500,
        title:"Add a new portfolio",
        buttons: [ { text: "Submit portfolio", click: function() { onNewParametersSubmit(); $(this).dialog("close"); } } ],
        modal:true,
        close: function( event, ui) { DestroyDialog(); }

    });
}

$(function() {
    $("#addnew_button").click(function() { 
        CreateDialog();
        $("#addnew_portfolio_dialog").dialog("open");
    });

});

function RefreshParent() { $("#sub_frame").load("/include/oleum_portfolios.php"); } 
</script>
</div>
</body>
</html>
