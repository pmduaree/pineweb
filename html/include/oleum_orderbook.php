<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="/jquery/jquery-ui-1.10.4.css">
<link rel="stylesheet" type="text/css" href="/css/oleum.css">
<script src="/jquery/jquery-1.10.2.js"></script>
<script src="/jquery/jquery-ui-1.10.4.js"></script>

</head>
<body>


<h2>Order Book</h2>

<div>
<label for="orderbook_asset">Select Portfolio</label>
<select id="orderbook_asset" style="width: 100px;">
<?php
require_once('sql_db.php');
$db = new sql_db('');

$query = "SELECT P.portfolio_id, I.name FROM Main.Instruments AS I JOIN Main.Portfolio AS P ON I.id = P.instrument_id WHERE P.portfolio_id > 0 ORDER BY P.portfolio_id ASC";
$result = $db->executeQuery($query);
//print_r($result);
$ids = array();
$s = '';

foreach($result as $X)
    array_push($ids, $X['portfolio_id']);

$portfolioids = array_unique($ids);

foreach($portfolioids as $X)
{
	$s = '';
	//print_r($result);

	foreach($result as $I)
	{
        if($I['portfolio_id'] == $X)
            $s .= sprintf('%s,', $I['name']);
    }
    //print $s;
    $s = substr($s, 0, -1);

    print sprintf('<option value="%s">%d</option>', $s, $X);
}
?>
</select>
</div>


<div id="orderbook_table">

</div>

<script>
function LoadTable()
{
    var asset = $("#orderbook_asset").val();
    console.log(asset);
    $("#orderbook_table").load('/scripts/load_orderbook.php?asset=' + asset);

}

$("#orderbook_asset").change(function() {
    LoadTable();
});

$(document).ready( function() {
    $("#orderbook_asset").val($("#orderbook_asset option:first").val());
    LoadTable();
});
</script>

</body>
</html>

