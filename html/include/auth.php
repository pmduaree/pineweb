<?php
session_start();

require_once('sql_db.php');

class UserDatabase
{
    function __construct()
    {
        $db = new sql_db('');
        $Z = $db->executeQuery('SELECT `username`,`password` FROM `pine_user_db`.`Users`');
        foreach($Z as $item) {
            $user = $item['username'];
            $md5pass = $item['password'];
            $this->users[$user] = $md5pass;
        }
    }
    
    function is_valid_user($user, $pass)
    {
        if (isset($this->users[$user]) && $this->users[$user] == md5($pass)) 
            return true;
        return false;
    }

    function save_user($user, $pass)
    {
        $db = new sql_db('');
        $q = sprintf("SELECT * FROM `pine_user_db`.`Users` WHERE `username`='%s'", $user);
        $my_user = $db->executeQuery($q);
        $md5pass = md5($pass);
        if (count($my_user) == 1) {
            $q = sprintf("UPDATE `pine_user_db`.`Users` SET `password`='%s' WHERE `username`='%s'",$md5pass,$user);
            $db->realQuery($q);
        } else {
            $q = sprintf("INSERT INTO `pine_user_db`.`Users` (`username`,`password`) VALUES ('%s','%s')", $user,$md5pass);
            $db->realQuery($q);
        }
    }
}

class LoginInfo
{
    function __construct()
    {
        $this->userdb = new UserDatabase();
    }


    function logged_in()
    {
        return ($_SESSION['valid_user']== true && isset($_SESSION['user']) && $_SESSION['user'] != '');
    }

    function clear()
    {
        $_SESSION['valid_user'] = false;
    }

    function login($user, $pass)
    {
        $suc = false;
        if ($this->userdb->is_valid_user($user, $pass) ) {
            $_SESSION['valid_user'] = true;
            $_SESSION['user'] = $user;
            $suc = true;
        } else {
            $_SESSION['valid_user'] = false;
            unset($_SESSION['user']);
        }
        return $suc;
    }

    function logout()
    {
        unset($_SESSION['user']);
        $_SESSION['valid_user'] = false;
    }

    function username() {
        return (isset($_SESSION['user']) ? $_SESSION['user'] : '');
    }

    function set_password($new_password)
    {
        $user = $this->username();
        if ($user == '') return;
        $this->userdb->save_user($user, $new_password);
    }


    function is_allowed($allowed_users)
    {
        if (!$this->logged_in() || !in_array($this->username(), $allowed_users)) {
            return false;
        } else
            return true;
    }

    public function __toString()
    {
        $s = '';
        $s .= '<div id="logininfo">';
        if ($this->logged_in()) {
            $s .= $this->username() . ' <a href="scripts/logout.php">Log out</a>';
        } else {
            $s .= '<a href="index.php">Log in</a>';
        }
        $s .= '</div>';
        return $s;
    }





}


?>
