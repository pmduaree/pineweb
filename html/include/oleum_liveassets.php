<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="/jquery/jquery-ui-1.10.4.css">
<link rel="stylesheet" type="text/css" href="/css/oleum.css">
<script src="/jquery/jquery-1.10.2.js"></script>
<script src="/jquery/jquery-ui-1.10.4.js"></script>
<script>function RefreshParent() { $("#sub_frame").load("/include/oleum_liveassets.php"); } </script>
</head>


<body>
<h2>Curent Live Assets</h2>

<?php
require_once('sql_db.php');
require_once('portfolio_selector.php');
require_once('trparam_selector.php');
$db = new sql_db('');
$query = "SELECT T.id, T.asset_id, T.strategyportfolio_id, T.traderpar_id, T.disabled,
                 I.name AS asset_name , 
                 SP.freq, SP.offset
          FROM `Main`.`Trading` AS T
          JOIN `Main`.`Instruments` AS I ON T.asset_id = I.id 
          JOIN `Main`.`StrategyPortfolios` AS SP ON T.strategyportfolio_id = SP.id";
$Z = $db->executeQuery($query);
$s = '';
if (!count($Z)) {
    $s .= 'No live assets defined';
    print $s;
} else  {
    $s .= '<div>';
    $s .= '<table>';
    $s .= '<tr>';
    $s .= '<th>id</th>';
    $s .= '<th>asset id</th>';
    $s .= '<th>asset</th>';
    $s .= '<th>strategy</th>';
    $s .= '<th>trader par</th>';
    $s .= '<th>period</th>';
    $s .= '<th>offset</th>';
    $s .= '<th></th>';
    $s .= '<th><img src="/img/Symb_add.png" name="symbol_add" class="symbol_add" onclick="onNewAsset();" /></th>';
    $s .= '</tr>';
    foreach($Z as $row) {
        $s .= '<tr>';
        $s .= sprintf('<td>%d</td>', $row['id']);
        $s .= sprintf('<td>%s</td>', $row['asset_id']);
        $s .= sprintf('<td>%s</td>', $row['asset_name']);
        $s .= sprintf('<td>%s</td>', $row['strategyportfolio_id']);
        $s .= sprintf('<td>%s</td>', $row['traderpar_id']);
        $s .= sprintf('<td>%s</td>', $row['freq']);
        $s .= sprintf('<td>%s</td>', $row['offset']);
        $s .= sprintf('<td><img src="/img/Symb_edit.png" name="symbol_edit" class="symbol_edit"  onclick="EditAsset(%d,%d,%d,%d)" /></td>', $row['id'],$row['asset_id'],$row['strategyportfolio_id'],$row['traderpar_id']);
        $s .= sprintf('<td><img src="/img/Symb_delete.png" name="symbol_delete" class="symbol_delete" onclick="RemoveAsset(%d)" /></td>', $row['id']);
        $s .= '</tr>';
    }
    $s .= '</table>';
    $s .= '</div>';
    print $s;
}
?>


<div id="addliveasset_window" style="display:none">
<div name="addnew_liveasset_subframe" id="addnew_liveasset_subframe">

<input readonly value="0" type="hidden" name="addnew_liveasset_id" id="addnew_liveasset_id" />
<div class="add_fld">
    <label for="addnew_liveasset_assetid">Asset</label>
    <select id="addnew_liveasset_assetid">
    <?php
    $query = "SELECT * FROM `Main`.`Instruments` WHERE `disabled`=0";
    $assets = $db->executeQuery($query);
    foreach($assets as $asset) {
        printf('<option value="%d">%s</option>', $asset['id'], $asset['name']);
    }
    ?>
    </select>
</div>
<div class="add_fld">
    <label for="addnew_liveasset_portfolioid">Strategy</label>
    <select id="addnew_liveasset_portfolioid">
    </select>
</div>


<br/>
<div class="add_fld">
    <label for="addnew_liveasset_trparamid">Trading parameters</label>
    <select id="addnew_liveasset_trparamid" style="width:800px">
    </select>
</div>


<script>
function LoadStrategiesforAsset()
{
    var asset = $("#addnew_liveasset_assetid").val();
    $("#addnew_liveasset_portfolioid").load("/scripts/load_strategies_for_asset.php?asset=" + asset, function() {
        $("#addnew_liveasset_portfolioid").val($("#addnew_liveasset_portfolioid option:first").val());
    });
    //alert("current loadedd strategy: " + $("addnew_liveasset_portfolioid").val() );
}

function LoadTrparamsforStrategy()
{
    var asset = $("#addnew_liveasset_assetid").val();
    var strategy = $("#addnew_liveasset_portfolioid").val();
    $("#addnew_liveasset_trparamid").load("/scripts/load_trparams_for_strategy.php?strategy=" + strategy + '&asset='+asset,
        function() { 
        $("#addnew_liveasset_trparamid").val($("#addnew_liveasset_trparamid option:first").val());
    });
}

$("#addnew_liveasset_assetid").change(function() { 
    LoadStrategiesforAsset(); 
    LoadTrparamsforStrategy();
} ); 
$("#addnew_liveasset_portfolioid").change(function() { 
    LoadTrparamsforStrategy(); 
} ); 


$(document).ready(function() {
    LoadStrategiesforAsset();
    LoadTrparamsforStrategy();
});


</script>


</div>  <!-- addnew_liveasset_subframe -->



<script>
function onNewTraderParametersSubmit() {
    var liveid = $("#addnew_liveasset_id").val();
    var assetid = $("#addnew_liveasset_assetid").val();
    var portfolioid = $("#addnew_liveasset_portfolioid").val();
    var trparamid = $("#addnew_liveasset_trparamid").val();
    var link = "/scripts/add_liveasset.php?liveid="+liveid+"&asset="+assetid+"&portfolio="+portfolioid+"&trparam="+ trparamid;
    $.get(link).done(
        function(data) {
            if (data.length > 10) alert(data); 
            RefreshParent();
        }
    );
}
</script>

</div> <!-- addliveasset_window -->



<script>
// initialize the dialog
$(function() {
    $("#addliveasset_window").dialog({
        width:900,height:300,title:"Live Asset",modal:true,
        appendTo:"#sub_frame",
        autoOpen: false,
    });
});


function EditAsset(id, assetid, portfolioid, trparamid) {
    $("#addnew_liveasset_id").val(id);
    $("#addnew_liveasset_assetid").val(assetid);
    $("#addnew_liveasset_portfolioid").val(portfolioid);
    $("#addnew_liveasset_trparamid").val(trparamid);
    $("#addliveasset_window").dialog('option', 'title', 'Edit live asset');
    $("#addliveasset_window").dialog('option', 'buttons', 
        [ {text : 'Update asset', click: function() { onNewTraderParametersSubmit(); $(this).dialog("close"); } } ]
    );
    $("#addliveasset_window").dialog('open');
}

function onNewAsset() {
    $("#addnew_liveasset_id").val("0");
    $("#addliveasset_window").dialog('option', 'title', 'Add live asset');
    $("#addliveasset_window").dialog('option', 'buttons', 
        [ {text : 'Add asset', click: function() { onNewTraderParametersSubmit(); $(this).dialog("close"); } } ]
    );
    $("#addliveasset_window").dialog('open');
}

function RemoveAsset(liveid) {
    var didConfirm = confirm("Are you sure you want to delete the live asset?");
    if (didConfirm == true) {
        var link = "/scripts/remove_liveasset.php?liveid="+liveid;
        $.get(link).done(
            function(data) {
                if (data.length > 10) alert(data); 
                RefreshParent();
            }
        );
    }
}
</script>



</body>
</html>


