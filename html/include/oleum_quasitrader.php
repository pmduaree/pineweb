<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="/jquery/jquery-ui-1.10.4.css">
<link rel="stylesheet" type="text/css" href="/css/oleum.css">
<script src="/jquery/jquery-1.10.2.js"></script>
<script src="/jquery/jquery-ui-1.10.4.js"></script>
<script type="text/javascript" src="http://code.highcharts.com/stock/highstock.js"></script>
<script type="text/javascript" src="http://code.highcharts.com/stock/modules/exporting.js"></script>
</head>

<body>

<?php
require_once('sql_db.php');
require_once('portfolio_selector.php');
?>

<h2>Quasi Trader</h2>


<div id="quasitrader_form">


<div class="add_fld">
<label for="portfolio_id">Strategy</label>
<select name="portfolio_id" id="portfolio_id">
<?php
$db = new sql_db('Main');
$strategies = $db->executeQuery('SELECT * FROM `Strategies`');
foreach($strategies as $S) {
    printf('<option value="%d">%d</option>', $S['id'], $S['id']);
}
?>
</select>
</div>

<div class="add_fld">
    <label for="quasitrader_date1">From date</label>
    <input id="quasitrader_date1" name="quasitrader_date1" onchange="storeVariable(this);" />
</div>

<div class="add_fld">
    <label for="quasitrader_date2">To date</label>
    <input id="quasitrader_date2" name="quasitrader_date2" onchange="storeVariable(this);"/>
</div>

<div class="add_fld">
    <label for="quasitrader_input">Input</label>
    <select id="quasitrader_input" name="quasitrader_input">
        <option value="close">Close price</option>
        <option value="mid">Mid price </option>
        <option value="spread">Bid/Ask Spread</option>

    </select>
</div>


<input id="quasitrader_submit" type="submit" value="Run" />
<button id="show_results" onclick="showResultsGraphs()">Show Results</button>

<script>
    $("#portfolio_id").change(function() { storeVariable(this) ;});
    $(document).ready( function() {
        $(".quasitrader_daterange").change(function() { storeVariable(this); } );
        $("#quasitrader_date1").datepicker({
            dateFormat: 'yy-mm-dd',
            defaultDate: "-2w -2d",
            maxDate: "-1d"
         });
        $("#quasitrader_date2").datepicker({
            dateFormat: 'yy-mm-dd',
            defaultDate: "-2d",
            maxDate: "-1d"
        });
    });
    
    $("#quasitrader_submit").click( function() { 
        runQuasiTrader(portfolio_id.value, quasitrader_date1.value, quasitrader_date2.value, $("#quasitrader_input").val());
    });
    
    function getVariableFullname(var_id) {
        var full_name = "oleum.quasitrader." + var_id;
        return full_name;
    }
    function storeVariable(obj) {
        var var_val = obj.value;
        var fullName = getVariableFullname(obj.name);
        localStorage[fullName] = var_val;
        //alert("Storing variable for " + fullName + " and it is: " + var_val);
    }
    
    function getVariable(objname) {
        //alert("loadVariable event for object " + objname );
        var fullName = getVariableFullname(objname);
        var var_val = localStorage[fullName];
        return var_val;
    }
    function loadVariable(obj) {
        //alert("loadVariable event for object " + obj.name );
        var fullName = getVariableFullname(obj.name);
        var var_val = localStorage[fullName];
        if (var_val) {
            obj.value = var_val;
        }
    }
</script>

</div>  <!-- quasitrader_form -->

<div>
<textarea id="output_area" readonly wrap="soft" name="output_area" cols="110" rows="25" onChange="storeVariable(this)" >
</textarea>
</div>

<div id="graphwindow" style="display:none">
    <div id="showgraph_profit" style="height:350px">
    </div>
    <div id="realizedRiskGraph" style="height:350px">
    </div>
    <div id="dollarExposureGraph" style="height:350px">
    </div>
    <hr>
<!--     <div id="showgraph_prices" style="height:350px">
    </div> -->
</div>
<script>
function showResultsGraphs()
{
    createWindowGraph();
    if($('#output_area').val() != undefined)
    {
        parseData();
        $("#graphwindow").dialog('open');
    }
}


function createWindowGraph()
{
    $("#graphwindow").dialog({
        width:650,height:400,title:"Results",modal:true,
        appendTo:"#sub_frame",
        autoOpen: false,
        close: function (event, ui) {  }
    });
}



function parseData()
{
    var data = $('#output_area').val().split("\n");
    //PROFIT ARRAY
    var profit_array = new Array();
    for(var i = 2; i < data.length-5; i+=1)
    {
        var aux = data[i].split(/[\s]+/g);
        var date_str = aux[0];
        var datestring = date_str.substr(1, date_str.length-2);
        var date = new Date(datestring).getTime();

        //profit_array[i-2] = [date , parseFloat((aux[4]-100000)/1000)];
        if(aux[1] == "EXIT" && aux[2] != 0)
            profit_array.push([date , parseFloat((aux[2])/100000)]);

    }
    createLineGraph('showgraph_profit', "Profit(%)" , profit_array);

    //RISK GRAPHS
    dollarStr = data[data.length-2];
    seriesStr = data[data.length-3];
    $.getJSON("/scripts/parse_string_json.php?string=\""+seriesStr+"\"&callback=?", function(data){
        //console.log(data);
        createBarGraph("realizedRiskGraph", "Realized Risk", data);
    });
    $.getJSON("/scripts/parse_string_json.php?string=\""+dollarStr+"\"&callback=?", function(data){
        createBarGraph("dollarExposureGraph", "Dollar Exposure", data);
    });


}

function createBarGraph(container, title, series)
{
    //console.log(title);
    var chart = new Highcharts.StockChart({
            chart: {
                renderTo: container,
                type: 'column'
            },
            title : {
                text : title,
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            xAxis: {
                categories: [
                    title
                ],
                ordinal: false,
            },
            exporting:  {
                enabled: true,
            },
            series : series
        });

}

function createLineGraph(container, title, data)
{
    //console.log(data);
    //if(typeof $('#'+container).highcharts() != 'undefined')
        //$('#'+container).highcharts().destroy();
    var chart = new Highcharts.StockChart({
            chart: {
                renderTo: container,
            },
            title : {
                text : title,
            },
            xAxis: {
                ordinal: false,
            },
            exporting:  {
                enabled: true,
            },
            series : [{
                name : title,
                data : data,
                marker : {
                    enabled : true,
                    radius : 3,
                },
                shadow : true,
                tooltip : {
                    valueDecimals : 2,
                },
            }]
        });
}


function onportfolioChange()
{
    storeVariable(portfolio_id);
    output_area.value = '';
    storeVariable(output_area);
}

$(function() {
    $('#portfolio_id').change(onportfolioChange);
    loadVariable(portfolio_id);
    loadVariable(quasitrader_date1);
    loadVariable(quasitrader_date2);
    loadVariable(output_area);
});

function runQuasiTrader(p_id, d_1,d_2, input) 
{
    output_area.value = "Running... Please wait. Do not go away, Do not refresh. Just wait please.";
    //console.log("../scripts/run_quasitrader.php?portfolio="+p_id+"&from="+d_1+"&to="+d_2+"&input="+input);
    $.get("../scripts/run_quasitrader.php?portfolio="+p_id+"&from="+d_1+"&to="+d_2+"&input="+input).done( 
        function(data) 
        { 
            output_area.value = data; 
            storeVariable(output_area);
        } 
    ); 
    
}
</script>

</body>
</html>
