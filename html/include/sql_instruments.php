<?php

require_once('sql_db.php');

class sql_instruments
{
    public $dbname;
    public $sqldb;
    function __construct($dbname='tix')
    {
        if ($dbname != 'tix') {
            print 'Sorry pal, wrong database.';
            die();
        }
        $this->dbname = $dbname;
        $this->sqldb = new sql_db($dbname);
    }

    public function load_instruments()
    {
        $query="SELECT * FROM `Main`.`Instruments`";
        $Z = array();
        if ($result = $this->sqldb->mysqli->query($query)) {
            while($row = $result->fetch_assoc()) {
                $Z[] = $row;
            }
            $result->close();
        }
        return $Z;
    }

    public function get_instrument($instrument_id)
    {
        $query=sprintf("SELECT * FROM `Main`.`Instruments` where id = %d", $instrument_id);
        $Z = array();
        if ($result = $this->sqldb->mysqli->query($query)) {
            while($row = $result->fetch_assoc()) {
                $Z[] = $row;
            }
            $result->close();
        }
        return $Z[0];
        //return $Z;
    }
    
    public function delete_instrument($instrument_id)
    {
        $query=sprintf("DELETE FROM `instruments` WHERE id=%d", $instrument_id);
        $res = $this->sqldb->mysqli->query($query);
        if ($res) $res->close();
    }

    public function find_instrument($instrument_name)
    {
        $query=sprintf("SELECT * FROM `instruments` WHERE `name` = %d", $instrument_name);
        $Z = array();
        if ($result = $this->sqldb->mysqli->query($query)) {
            while($row = $result->fetch_assoc()) {
                $Z[] = $row;
            }
            $result->close();
        }
        return $Z;
    }

    public function add_instrument($I)
    {
        $query = sprintf("INSERT INTO `Main`.`Instruments`  
                         (`name`, `exchange`, `description`, `source`,`type`,`disabled`,
                          `open_market`, `commission`, `lotsize`, `margin`,`trading_day`) 
                         VALUES ('%s','%s','%s','%s','%d','0','%s','%s','%d','%.4f','%s')", 
                         $I['name'],$I['exchange'], $I['description'],$I['source'],
                         $I['type'],$I['open_market'],$I['commission'],$I['lotsize'],
                         $I['margin'],$I['trading_day']);
        #print 'query:' . $query;
        $res = $this->sqldb->mysqli->query($query);
        if ($res) $res->close();
    }
}

?>
