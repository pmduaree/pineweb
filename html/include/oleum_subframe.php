<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="/jquery/jquery-ui-1.10.4.css">
<link rel="stylesheet" type="text/css" href="/css/oleum.css">
<script src="/jquery/jquery-1.10.2.js"></script>
<script src="/jquery/jquery-ui-1.10.4.js"></script>
</head>

<body>

<script>
$(document).ready(function() { 
$(".xdsoft_datetimepicker").remove();
});
</script>

<?php


$subframe=$_GET['subframe'];

$DATA = array( 'oleum_subframe_trading' => array( 'oleum_autoexecution' => 'Autoexecution', 'oleum_orderbook'=>'Order Book', 'oleum_quasitrader'=>'Quasi Trader'),
               'oleum_subframe_signals' => array( 'oleum_signals_strategy' => 'Signals', /*'oleum_signals_indicator'=> 'Indicators'*/),
               'oleum_subframe_conf'    => array( 'oleum_strategies' => 'Strategies', /*'oleum_trparams'=>'Trading',*/ 'oleum_portfolios' => 'Portfolios' , 'pine_kalman' => 'Kalman\'s Portfolio' ,),
               'oleum_subframe_data'    => array( 'assetdata' => 'Candles', 'ticks' => 'Bid/Ask', 'pine_artificial' => 'Artificial Data',)
            );
                


print '<div id="top_bar" class="oleum_toolbar">';
foreach($DATA[$subframe] as $k => $v) {
    printf('<div id="topbutton_%s" class="oleum_button top_button" name="%s">%s</div>', $k, $k, $v);
}
print '</div>';


print '<div id="sub_frame"></div>';

$akeys = array_keys($DATA[$subframe]);
$default_frame =  $akeys[0];

printf('<script>
function loadPage(myname) {
    $(".xdsoft_datetimepicker").remove();
    $("#ui-datepicker-div").remove();
    sessionStorage.setItem("oleum.%s.sel", myname);
    var page = "/include/" + myname + ".php";
    $("#sub_frame").load(page);
    var my_topbutton = "topbutton_" + myname;
    $(".top_button").css( "border", "1px solid white");
    $("#" + my_topbutton).css("border","1px solid #777777");
}

$(".top_button").click( function() {
    loadPage(this.getAttribute("name"));
});

$(document).ready( function() { 
    var cursel = sessionStorage.getItem("oleum.%s.sel");
    if (!cursel && typeof cursel != \'string\') {
        cursel = "%s";
    }
    loadPage(cursel);
});
</script>', $subframe, $subframe, $default_frame);

?>

</body>
</html>

