<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="/jquery/jquery-ui-1.10.4.css">
<link rel="stylesheet" type="text/css" href="/css/oleum.css">
<script src="/jquery/jquery-1.10.2.js"></script>
<script src="/jquery/jquery-ui-1.10.4.js"></script>
</head>

<body>

<h2>Indicator Collection</h2>

<div id="oleum_indicators_straegy_selector_dropbox">



<?php
require_once('sql_db.php');
?>


<?php
class StrategySelector
{
    public function __toString()
    {
        $db = new sql_db('');
        $Z = $db->execute('SELECT * FROM `Main`.`Strategies` ORDER BY `type` ASC, `freq` DESC');

        $strategy_types = $db->executeQuery('SELECT `type` FROM `Main`.`StrategyTypes` ORDER BY `id` ASC');
        $strats = array();
        foreach($strategy_types as $st) {
            $type = $st['type'];
            $strats[$type] = array();
        }

        foreach($Z as $zz) {
            $strats[$zz['type']] [] = $zz;
        }
        $s = '';

        $s .= '<label for="strategy_type">Select the indicator type: </label>';
        $s .= '<select style="width: 100px;" name="strategy_type" id="strategy_type">';
        foreach(array_keys($strats) as $stt) {
            $s .= sprintf('<option name="%s">%s</option>', $stt, $stt);
        }
        $s .= '</select>';

        $s .= sprintf('<table id="strategytable">', $tableid);
        $s .= '<tr>';
        $s .= '<th style="width: 120px;">Type</th>';
        $s .= '<th style="width: 50px;">Freq</th>';
        $s .= '<th style="width: 770px;">Param</th>';
        $s .= '<th style="width: 30px;"><img src="/img/Symb_add.png" name="symbol_add" class="symbol_add" id="button_new_indicator" /></th>';
        $s .= '</tr>';
        foreach($strats as $stt => $stv) {
            foreach($stv as $row) {
                $s .= '<tr style="display: none; ">';
                $s .= sprintf('<td style="width: 120px;" >%s</td>', $stt);
                $s .= sprintf('<td style="width: 50px;" >%d</td>', $row['freq']);
                $s .= sprintf('<td>%s</td>', $row['param']);
                $s .= sprintf('<td><img src="/img/Symb_delete.png" name="symbol_delete" class="symbol_delete" onclick="DeleteIndicator(%d)" />', $row['id']);
                $s .= '</tr>';
            }
        }
        $s .= '</table>';

        $s .= "\n\n";
        $s .= '<script>function DeleteIndicator(indicatorid) { 
            var didConfirm = confirm("Are you sure you want to delete the indicator?");
            if (didConfirm == true) {
                var link="/scripts/remove_indicator.php?id=" + indicatorid;
                $.get(link).done( function(data) { 
                    if (data.length > 10) alert(data); 
                    RefreshParent();
                });
            }
        }
        </script>';
        $s .= '<script>function RefreshStrategyTable() {
            var curval = $("#strategy_type").val();
            $("#strategytable tr:not(:first)").each(
                function() { 
                    var tt = $(this.cells[0]).html();
                    if (tt == curval) {
                        $(this).show();
                    } else {
                        $(this).hide();
                    }
                }
            );

        }</script>';
        $s .= '<script>
            $("#strategy_type").on("change", function() {
                RefreshStrategyTable();
            });
        </script>';
        $s .= '<script>$("#strategytable").ready(function() { RefreshStrategyTable(); });</script>';

        return $s;
    }
}
$stratesel = new StrategySelector();
print $stratesel;
?>
</div>

<div id="newstrategy_popup" style="display:none">
<div name="addnew_subframe" id="addnew_subframe">
<?php
    // read strategies from the DATbaASE
    $mydb = new sql_db('');
    $strategy_types = $mydb->executeQuery('SELECT * FROM `Main`.`StrategyTypes`');
    $Sparam = array();
    foreach($strategy_types as $st) {
        $fields = explode(',', $st['fields']);
        $type = $st['type'];
        $Sparam[$type] = $fields;
    }
    $s .= '<div class="add_fld">';
    $s .= '<label for="addnewstrat_type">Type</label>';
    $s .= '<select name="addnewstrat_type" id="addnewstrat_type">';
    foreach(array_keys($Sparam) as $ktype) {
        $s .= sprintf('<option value="%s">%s</option>', $ktype, $ktype);
    }
    $s .= '</select>';
    $s .= '</div>';
    $s .= '<div class="add_fld">';
    $s .= '<label for="addnewstrat_freq">Period</label>';
    $s .= '<input type="number" name="addnewstrat_freq" id="addnewstrat_freq" min="5" max="60" step="5" value="30">';
    $s .= '</div>';
    $s .= '<br/>';
    $s .= '<div id="addnewstrat_parameters" style="border: 1px solid black ; height: 150px;" >';
    foreach($Sparam as $ktype => $xparams) {
        $subform_name = sprintf('subform_%s', $ktype);
        $s .= sprintf('<div id="%s" name=%s" style="display: none" >', $subform_name, $subform_name);
        foreach($xparams as $v) {
            $field_name=sprintf("%s_%s", $subform_name, $v);
            $s .= '<div class="add_fld">';
            $s .= sprintf('<label for="%s">%s</label><input name="%s" id="%s" style="width: 80px" >', $field_name, $v, $field_name, $field_name);
            $s .= '</div>';
        }
        $s .= '</div>';
    }
    $s .= '</div>';
    print $s;
?>
</div>

<script>
function RefreshNewStratParams() {
    var cursel = $("#addnewstrat_type").val();
    var the_right_form = "subform_" + cursel;
    $("#addnewstrat_parameters").children("div").each(function() { $(this).hide(); } );
    $("#" + the_right_form).show();
}
    
$("#addnewstrat_type").on("change", function() { RefreshNewStratParams(); });
$("#addnewstrat_submit").ready(function() { RefreshNewStratParams(); });
$("#addnewstrat_submit").on("click", function() { onNewIndicatorSubmit(); });
</script>
</div> <!-- newstrategy_popup -->

<script>

function DestroyDialog()
{
    $("#newstrategy_popup").dialog('destroy');
}


function CreateDialog()
{
    $("#newstrategy_popup").dialog({
        modal: true,
        appendTo: "#sub_frame",
        autoOpen: false,
        width: 800,
        height: 400,
        title: "Add a new indicator",
        buttons: [ {text:'Add indicator', click: function() { onNewIndicatorSubmit(); $(this).dialog('close') } },
                   ],
        modal:true,
        close: function( event, ui) { DestroyDialog(); }
    });
}

$(function() {
    $("#button_new_indicator").click( function() {
        CreateDialog();
        $("#newstrategy_popup").dialog("open");
    });

});

function onNewIndicatorSubmit() {
    var cursel = $("#addnewstrat_type").val();
    var freq = $("#addnewstrat_freq").val();
    var the_right_form = "subform_" + cursel;
    var z = "";
    $("#" + the_right_form).children("div").each( function() { 
            $(this).children("input").each( 
            function() {
                var name = $(this).attr("name");
                name = name.substring(the_right_form.length+1);
                z = z + name + "=" + $(this).val() + ",";
            })
    });
    var param = z.substring(0, z.length - 1);
    //alert("straetegy: " + cursel + " with param: " + param);
    link="/scripts/add_indicator.php?type=" + cursel + "&param=\'" + param + "\'&freq=" + freq;
    $.get(link).done( function(data) { 
        if (data.length > 10) alert(data); 
        RefreshParent();
    });
}

function RefreshParent() { 
    //sessionStorage.SetItem('oleum.indicator.dropbox.selection', $("#strategy_type").val() );
    $("#sub_frame").load("/include/oleum_indicators.php"); 
    //$("#strategy_type").val( sessionStorage.GetItem('oleum.indicator.dropbox.selection') );
    //sessionStorage.removeItem('oleum.indicator.dropbox.selection');

} 
</script>



</body>
</html>
