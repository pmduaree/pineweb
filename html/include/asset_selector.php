<?php

require_once('sql_db.php');

class AssetSelector
{
    function __construct($myid='asset_id')
    {
        $this->id = $myid;
        $db = new sql_db('');
        $query = "SELECT * FROM `Main`.`Instruments` WHERE `disabled`=0";
        $this->ZZ = $db->executeQuery($query);
    }

    public function __toString()
    {
        $s = '';
        $s .= sprintf('<select name="%s" id="%s">', $this->id, $this->id, $this->id);
        foreach($this->ZZ as $I) {
            $s .= sprintf('<option value="%d">%s</option>', $I['id'], $I['name']);
        }
        $s .= '</select>';
        return $s;
    }
}

?>
