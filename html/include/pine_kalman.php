<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="/jquery/jquery-ui-1.10.4.css">
<link rel="stylesheet" type="text/css" href="/css/oleum.css">
<script src="/jquery/jquery-1.10.2.js"></script>
<script src="/jquery/jquery-ui-1.10.4.js"></script>
<script type="text/javascript" src="http://code.highcharts.com/stock/highstock.js"></script>
<script type="text/javascript" src="http://code.highcharts.com/stock/modules/exporting.js"></script>
<script>function RefreshParent() { $("#sub_frame").load("/include/pine_kalman.php"); } </script>
</head>

<body>


<div id="current_assets">
<h2> Assets in the Kalman's portfolio: </h2>
<div>
<form>
<?php
require_once('sql_db.php');

$db = new sql_db('');
$X = $db->execute('SELECT instrument_id FROM Main.Portfolio WHERE portfolio_id = 0');
$Z = $db->execute('SELECT id, name, description FROM `Main`.`Instruments`');
$ids = array();
foreach($X as $x)
    $ids[] = $x['instrument_id'];
foreach($Z as $I) {
    printf('<input type="checkbox" name="assets" value="%d" %s>%s<br>',
     $I['id'], in_array($I['id'], $ids) ? "checked" : "" ,$I['description']);
}
?>
</form>
</div>


<div>
<input type="submit" id="submit_kalman" value="Update Kalman" onclick="update_kalman()" />
</div>



</div>


<script>
function update_kalman() { 
    var assets = document.forms[0].assets;
    var txt = "";
    var i;
    for (i = 0; i < assets.length; i++) 
        if (assets[i].checked) 
            txt = txt + assets[i].value + ",";
    var link = "../scripts/portfolio_kalman.php?assets="+txt.substring(0,txt.length-1);
//    console.log(link);
    $.get(link).done(
        function(data) {
            if (data.length > 10) alert(data); 
            RefreshParent();
        }
    );
}
</script>

</body>
</html>
