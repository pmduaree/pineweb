<?php

require_once "jshelper.php";

/**
 *
 * Class for using HighChart (jQuery plugin)
 * @author vbotalov
 *
 */
class HighChart {
	private $container;
	private $data;
	private $title;

	/**
	 *
	 * Create HighChart data object.
	 * @param string $title - title used to show data.
	 * @param arrya $data - array to display/
	 */
	public function __construct($container, $title, $data, $type) {
		$this->container = $container;
		$this->data = $data;
		$this->title = $title;
		$this->type = $type;
	}

	/**
	 *
	 * Return string with JavaScript used to create
	 */
	public function __toString() {
		$dataArray = JSHelper::generate2DArray($this->data);

		$titleObj = array();
		$titleObj["text"] = "'".$this->title."'";
		$titleObjStr = JSHelper::generateObj($titleObj);

		$seriaObj = array();
		$seriaObj["type"] = "'".$this->type."'";
		$seriaObj['name'] = "'".$this->title."'";
		$seriaObj["data"] = $dataArray;
		$seriaObjStr = JSHelper::generateObj($seriaObj);

        $xAxisObj = array();
        $xAxisObj["ordinal"] = "false";
        $xAxisObjStr = JSHelper::generateObj($xAxisObj);

        $outerObj = array();
        $outerObj["xAxis"] = $xAxisObjStr;
		$outerObj["title"] = $titleObjStr;
		$outerObj["series"] = "[".$seriaObjStr."]";
		$outerObjStr = JSHelper::generateObj($outerObj);

		$params = array();
		$params[] = "'StockChart'";
		$params[] = $outerObjStr;
		$paramStr = JSHelper::generateParamsStr($params);

		$selector = "'#".$this->container."'";
		$callback = JSHelper::generateCall(JSHelper::generateJQueryObj($selector), 'highcharts', $paramStr);

		return $callback;//JSHelper::generateLoaded($callback);
	}


}

?>
