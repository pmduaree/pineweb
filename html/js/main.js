/**
 * 
 */

var mainWindowHelper = {
	init : function(containerId) {
		$(window).load(function() {
			setTimeout(function() {
				mainWindowHelper.resizeWindow(containerId);
			}, 200);
		});
		$(window).resize(function() {
			mainWindowHelper.resizeWindow(containerId);
		});
	},
	resizeWindow : function(containerId) {
		var width = $(window).width() - 240;
		if (width > 700) {
			width = 700;
		}
		var height = $(window).height() - 40;
		var chart = $(containerId).highcharts();
		$(containerId).css("height", (height + 20) + "px");
		$(containerId).css("width", (width + 20) + "px");
		chart.setSize(width, height, false);
		
		$(".newsInner").css("height", (height + 10) + "px");
	}
};
mainWindowHelper.init("#sampleGraph");