<?php
//////////////////////////////////////////////////////////////////////////////
// localdata.php
//
// Global-view Emulator. 
//  Basically it does the same as ./globalview.php, it takes the same get
//  arguments. And it spits out the time series, just that it doesn't ask
//  globalview for data but it fetches the data from teh SQL database.
// 
// Example:
//  ./localdata.php?instrument_id=/BRN&start_date=2014-03-01 08:00:00&end_date=2014-03-10 12:00:00&interval=60
// 
// Author: Iztok Pizorn <iztok.p@oleum.com>
// 
// This is proprietary software.
// 
//////////////////////////////////////////////////////////////////////////////

// we don't allow queries without the instrument_id.
if (!isset($_GET['instrument_id'])) {
    print '<html>';
    print '<body>';
    print '</body>';
    print '</html>';
    die();
}
// actually we don't care if you provide the correct arguments or not.
// if you supply rubbish, that's also what you'll get.

$instrument_name = $_REQUEST['instrument_id'];
$start_date = $_REQUEST['start_date'];
$end_date = $_REQUEST['end_date'];
$interval = $_REQUEST['timestamp'];


$basedir = dirname(__FILE__);
require_once($basedir . '/include/sql_instruments.php');
require_once($basedir . '/include/sql_rawdata.php');


$instruments_db = new sql_instruments();
$instrument = $instruments_db->find_instrument($instrument_name);
if (!count($instrument)) die();

$instrument_id = $instrument[0]['id'];

// get the data.
$rawdata = new sql_rawdata();
$data = $rawdata->get_in_range($instrument_id, $interval, $start_date, $end_date);
$legend = array();

function make_an_array($z)
{
    $zz = array();
    foreach($z as $zi) $zz[] = '"' . $zi . '"';
    $s = implode(',', $zz);
    return $s;
}


print '[';
$fields=array("INSTRUMENT_ID","OPEN","HIGH","LOW","CLOSE","VOLUME","TRADE_DATETIME");
print '[' . make_an_array($fields) . ']';
if (count($data)) print ',';

for($i=0;$i<count($data);$i++) {
    $item = $data[$i];
    $z = array();
    $z['INSTRUMENT_ID'] = $instrument_name;
    $z['HIGH'] = $item['high'];
    $z['OPEN'] = $item['open'];
    $z['CLOSE'] = $item['close'];
    $z['LOW'] = $item['low'];
    $z['VOLUME'] = $item['volume'];
    $mydate_x = explode(" ", $item['date']);
    assert( count($mydate_x) == 2);
    $mydate = $mydate_x[0] . 'T' . $mydate_x[1] . 'Z';
    $z['TRADE_DATETIME'] = $mydate;

    $zt = array();
    foreach($fields as $field) $zt[] = '"' . $z[$field] . '"';
    $line= '[' . implode(',', $zt) . ']';

    print $line;
    if ($i < count($data)-1) print ',';
}

print ']';



?>
