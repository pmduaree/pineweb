<?php
session_start();

require_once('../include/auth.php');

$allowed_users = array('iztok');
$login = new LoginInfo();
if(!in_array($login->username(), $allowed_users) ) {
    print 'You are not allowed to add a new indicator type';
    exit();
}

$itype = $_POST['type'];
$idesc = $_POST['desc'];
$iparams = $_POST['params'];

require_once('../include/sql_db.php');

$db = new sql_db('');
$query = sprintf('INSERT INTO `Main`.`StrategyTypes` (`type`,`info`,`fields`) VALUES ("%s","%s","%s")', $itype, $idesc, $iparams);
$db->realQuery($query);

?>
