<?php

error_reporting(E_ALL);
ini_set('display_errors', True);

$basedir = dirname(__FILE__);
$include_dir = $basedir . '/../include';

require_once($include_dir . '/auth.php');

$allowed_users = array('iztok', 'abraham','juan','pedro','david','senatore');
$login = new LoginInfo();
if (!in_array($login->username(), $allowed_users)) {
    print 'Permission denied.';
    exit();
}

require_once($include_dir . '/sql_db.php');

$db = new sql_db('');

$assets = explode(",", $_GET['assets']);
//print_r($assets);

$db->realquery("DELETE FROM Main.Portfolio WHERE portfolio_id = 0");

$query = 'INSERT INTO `Main`.`Portfolio` (`portfolio_id`,`instrument_id`,`order`) VALUES ';
$order = 1;
foreach ($assets as $X)
	$query .= sprintf('(%d,%d,%d),', 0, $X, $order++);
//print $query;
$query = substr($query, 0, -1).";";
$db->realquery($query);
?>
