<?php

error_reporting(E_ALL);
ini_set('display_errors', True);

$basedir = dirname(__FILE__);
$include_dir = $basedir . '/../include';

require_once($include_dir . '/auth.php');

$allowed_users = array('iztok', 'abraham','ryan','pedro', 'david', 'senatore');
$login = new LoginInfo();
if (!in_array($login->username(), $allowed_users)) {
    print 'Permission denied.';
    exit();
}


$instrument_id = $_GET['id'];
$instrument_name = $_GET['instrument_name'];
$ticker = $_GET['ticker'];
$instrument_desc = $_GET['instrument_desc'];
$source = $_GET['source'];
$open_market = $_GET['open_market'];
$trading_day = $_GET['trading_day'];
$commission = $_GET['commission'];
$lotsize = $_GET['lotsize'];
$margin = $_GET['margin'];


$query = '';
if (!$instrument_id) {
    $query = sprintf('INSERT INTO `Main`.`Instruments` (`name`,`ticker`,`description`,`source`,`type`,`open_market`,`trading_day`,`commission`,`lotsize`,`margin`)
                      VALUES ("%s","%s","%s","%s",%d,"%s","%s","%s",%d,%.5f)', 
                     $instrument_name, $ticker, $instrument_desc, $source, 0, $open_market, $trading_day, $commission, $lotsize, $margin);
} else {
    $query = sprintf('UPDATE `Main`.`Instruments` SET `name`="%s",`ticker`="%s",`description`="%s",`source`="%s",`type`=%d,`open_market`="%s",`trading_day`="%s",`commission`="%s",`lotsize`=%d,`margin`=%.5f
                      WHERE `id`=%d', 
                     $instrument_name, $ticker,  $instrument_desc, $source, 0, $open_market, $trading_day, $commission, $lotsize, $margin, $instrument_id);
}

require_once($include_dir . '/sql_db.php');

$db = new sql_db('');
$db->realQuery($query);

?>
