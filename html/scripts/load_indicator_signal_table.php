<?php

$asset = $_GET['asset'];
$itype = $_GET['itype'];
$strategy_id = $_GET['strategy'];
$howmany = 25;

require_once('../include/sql_db.php');
$db = new sql_db('');


$query = '';
if ($strategy_id > 0) {
    $query = sprintf('SELECT SIG.*, 
                             II.name as asset_name,
                             STRAT.type as strategy_type,
                             STRAT.freq as strategy_freq
                            FROM `Main`.`Signals` as SIG
                        JOIN `Main`.`Instruments` AS II ON SIG.asset_id = II.id
                        JOIN `Main`.`Strategies` AS STRAT ON SIG.strategy_id = STRAT.id
                      WHERE asset_id = %d AND STRAT.id = %d
                      ORDER BY date DESC LIMIT %d', $asset, $strategy_id, $howmany);

} else {
    $query = sprintf('SELECT SIG.*, 
                             II.name as asset_name,
                             STRAT.type as strategy_type,
                             STRAT.freq as strategy_freq
                            FROM `Main`.`Signals` as SIG
                        JOIN `Main`.`Instruments` AS II ON SIG.asset_id = II.id
                        JOIN `Main`.`Strategies` AS STRAT ON SIG.strategy_id = STRAT.id
                      WHERE asset_id = %d AND STRAT.type = \'%s\'
                      ORDER BY date DESC LIMIT %d', $asset, $itype, $howmany);


}
$Z = $db->executeQuery($query);


function int_to_signal($i)
{
    if ($i==0) return 'None';
    if ($i==1) return 'Wait';
    if ($i==2) return 'Buy';
    if ($i==3) return 'Sell';
    return 'Error';
}


print '<table border="1px">';
print '<tr>';
print '<th>Strategy</th>';
print '<th>Asset</th>';
print '<th>Date</th>';
print '<th>Buy Prob</th>';
print '<th>R</th>';
print '</tr>';
foreach($Z as $zz) {
    print '<tr>';
    $strategy = sprintf("%d [%s,period=%d]", $zz['strategy_id'], $zz['strategy_type'], $zz['strategy_freq']);
    print sprintf('<td>%s</td>', $strategy);
    print sprintf('<td>%s</td>', $zz['asset_name']);
    print sprintf('<td>%s</td>', $zz['date']);
    print sprintf('<td>%.3f</td>', $zz['Ppos']);
    print sprintf('<td>%s</td>', $zz['Rpar']);
    print '</tr>';
}
print '</table>';


?>
