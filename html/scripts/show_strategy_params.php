<?php

error_reporting(E_ALL);
ini_set('display_errors', True);

include ("../include/sql_db.php");

$db = new sql_db('');
$id = $_GET['id'];



print "<form>";
//CONTROL OF THE PARAMETERES
$query = sprintf("SELECT P.Name, P.MinVal, P.MaxVal, T.Name AS type, P.default FROM Main.SignalerEnginesParam AS P JOIN Main.ParameterTypes AS T ON P.type = T.id AND P.SignalerType_id = %d", $id);
$params_config_array = $db->executeQuery($query);
foreach($params_config_array as $P)
{
	print '<div class="add_fld">';
    printf('<label for="addnew_param">%s</label>', $P['Name']);	
	switch($P['type'])
	{
		case "Bool":

			break;
		case "Int":
		case "Double":
        	printf('<input type="number" min="%d" max="%d" step="5" name="param" id="%s" value="%d" />',
        		$P['MinVal'], $P['MaxVal'], $P['Name'], ($P['MinVal'] + $P['MaxVal'])/2);
			break;
		case "String":
        	printf('<input name="param" id="%s" value="%s" />', $P['Name'], $P['default']);
			break;
    };
    print '</div>';
}
print "</form>";
//CONTROL OF THE ASSETS AND PORTFOLIOS
$query = sprintf("SELECT assetType FROM Main.SignalerType WHERE id = %d", $id);
$assetType = $db->executeQuery($query);
$assetType = $assetType[0]['assetType'];
print '<div class="add_fld">';
switch($assetType)
{
	case 0: //portfolios
        print '<label for="addnew_portfolio">Portfolio</label>';
        print '<select id="addnew_portfolio" name="addnew_portfolio">';
		$II = $db->executeQuery("SELECT portfolio_id FROM `Main`.`Portfolio` WHERE portfolio_id > 0 GROUP BY portfolio_id");
        foreach($II as $I) {
            printf('<option value="%d">%s</option>', $I['portfolio_id'], $I['portfolio_id']);
        }
        print '</select>';
		break;
	case 1: //single assets
	    print '<label for="addnew_portfolio">Asset</label>';
        print '<select id="addnew_portfolio" name="addnew_portfolio">';
		$II = $db->executeQuery("SELECT id, name FROM Main.Instruments");
        foreach($II as $I) {
            printf('<option value="%d">%s</option>', -1*$I['id'], $I['name']);
        }
        print '</select>';
		break;
}
print "</div>";

?>