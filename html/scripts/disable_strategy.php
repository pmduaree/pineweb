<?php

error_reporting(E_ALL);
ini_set('display_errors', True);

$basedir = dirname(__FILE__);
$include_dir = $basedir . '/../include';

require_once($include_dir . '/auth.php');

$allowed_users = array('iztok', 'abraham','ryan','pedro','david','senatore');
$login = new LoginInfo();
if (!in_array($login->username(), $allowed_users)) {
    print 'Permission denied.';
    exit();
}


$disabled = $_GET['disabled'];
$id = $_GET['id'];

$query = sprintf('UPDATE `Main`.`Strategies` SET `disabled`=%d WHERE `id`=%d', $disabled, $id);

require_once($include_dir . '/sql_db.php');

$db = new sql_db('');
$db->realQuery($query);

?>
