<?php
session_start();
require_once(dirname(__FILE__) . '/../include/auth.php');
$logininfo = new LoginInfo();
$logininfo->logout();
header('Location: ../index.php');
?>
