<?php

$basedir = dirname(__FILE__);
require_once($basedir . '/../include/auth.php');

$allowed_users = array('iztok','ryan','abraham');

$login = new LoginInfo();
if (!in_array($login->username(), $allowed_users)) {
    print 'Sorry. You do not have permission to add a new trader parameter set';
    exit();
}


require_once($basedir . '/../include/sql_db.php');

$date1 = str_replace("'","",$_GET['date1']);
$date2 = str_replace("'","",$_GET['date2']);
$date1 = sprintf('%s 01:00', $date1);
$date2 = sprintf('%s 23:00', $date2);
$taskinfo=sprintf("asset=%d,portfolio=%d,from=%s,to=%s,ntrials=%d",  
                  $_GET['asset'],$_GET['portfolio'],$date1,$date2,$_GET['ntrials']);
$query = sprintf('INSERT INTO `Main`.`TraderOptimization` (`note`) VALUES (\'%s\')', $taskinfo);
#print $query;

$db = new sql_db('Main');
$db->mysqli->real_query($query);

?>
