<?php
session_start();
require_once('../include/auth.php');
$allowed_users = array('iztok','ryan','abraham');
$login = new LoginInfo();
if (!in_array($login->username(), $allowed_users) ) {
    print "Sorry. You do not have a permission to add an indicator.";
    exit();
}

$strategy_type = $_GET['type'];
$strategy_param = $_GET['param'];
$strategy_freq = $_GET['freq'];
$strategy_param = str_replace("'", "", $strategy_param);

require_once('../include/sql_db.php');

$db = new sql_db('');
$query = sprintf('INSERT INTO `Main`.`Strategies` (`type`,`param`,`freq`) VALUES (\'%s\',\'%s\',%d)',
                 $strategy_type, $strategy_param, $strategy_freq);
$db->realQuery($query);

?>
