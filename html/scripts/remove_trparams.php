<?php
require_once('../include/auth.php');
$login = new LoginInfo();
$allowed_users = array('iztok');
if (!in_array($login->username(), $allowed_users)) {
    print 'Sorry. You are not allowed to delete trading parameters.';
    exit();
}

$trparamid = $_GET['id'];
require_once('../include/sql_db.php');
$db = new sql_db('');
$query = sprintf('DELETE FROM `Main`.`TraderParameters` WHERE `id`=%d', $trparamid);
$db->realQuery($query);
?>

