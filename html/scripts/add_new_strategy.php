<?php

error_reporting(E_ALL);
ini_set('display_errors', True);

$basedir = dirname(__FILE__);
$include_dir = $basedir . '/../include';

require_once($include_dir . '/auth.php');

$allowed_users = array('iztok', 'abraham','ryan','pedro','david','senatore');
$login = new LoginInfo();
if (!in_array($login->username(), $allowed_users)) {
    print 'Permission denied.';
    exit();
}



require_once($include_dir . '/sql_db.php');

$db = new sql_db('');

$indicator = $_GET['indicator'];
$param = $_GET['param'];
$freq = $_GET['freq'];
$port = $_GET['port'];


//save the params 
$query = sprintf('INSERT INTO Main.SignalerEngines(CurrentParam, Type_id) VALUES ("%s", %d);', $param, $indicator);
$db->realQuery($query);

//relation the param to the strategy itself

//get the last id
$query = sprintf('SELECT MAX(id) AS id FROM Main.SignalerEngines');
$aux = $db->executeQuery($query);
$last_id = $aux[0]['id'];

//This is only for the single asset strategies
if($port < 0)
{
	//checks if the portfolio already exists, otherwise creates it
	$port *= -1;
	$query = sprintf('
		SELECT P.portfolio_id FROM (
			SELECT portfolio_id FROM Main.Portfolio GROUP BY portfolio_id HAVING COUNT(*) = 1
		) x JOIN Main.Portfolio AS P WHERE x.portfolio_id = P.portfolio_id AND P.instrument_id = %d',
		$port);
	$aux = $db->executeQuery($query);
	if($aux) //if it exits
		$port = $aux[0]['portfolio_id'];
	else //otherwise, create a new portfolio containing the single asset 
	{
		$next_index = $db->executeQuery("SELECT MAX(portfolio_id)+1 as next_index FROM Main.Portfolio");
		$next_index = $next_index[0]['next_index'];
		$query = sprintf('INSERT INTO `Main`.`Portfolio` (`portfolio_id`,`instrument_id`,`order`) VALUES (%d, %d, 1);', 
			$next_index, $port);
		$db->realQuery($query);
		$port = $next_index;
	}
	print $port;
}

$query = sprintf('INSERT INTO Main.Strategies 
	(SignalerEngine_id, freq , disabled , LiveTracking ,portfolio_id, EncoderEngine_id, InvestingMoney)
    VALUES (%d, %d, %d, %d, %d, %d, %d)', 
    $last_id, $freq, 1, 0, $port, $indicator, 10000);
$db->realQuery($query);

?>
