<?php
$portfolio_id = $_GET['strategy'];
$howmany = $_GET['howmany'];

if ($howmany < 10) $howmany = 10;

require_once('../include/sql_db.php');

$db = new sql_db('');

$query = sprintf('SELECT S.date, S.id, S.asset_id, I.name, S.side, S.processed FROM `Main`.`Signal`  AS S 
                  JOIN `Main`.`Instruments` AS I ON S.asset_id = I.id
                  WHERE S.`portfolio_id`=%d ORDER BY S.`date`  DESC LIMIT %d', $portfolio_id, $howmany);
$Z = $db->executeQuery($query);

function int_to_signal($i)
{
    if ($i==0) return 'Wait';
    if ($i==1) return 'Buy';
    if ($i==-1) return 'Sell';
    return 'Error';
}

// '<table>';
$print_array = array();
$current_id = 0;
foreach($Z as $zz) 
    $print_array[$zz['asset_id']][] = $zz;//[] = $zz;

//print_r($print_array);
if($print_array)
    foreach($print_array as $Y)
    {
            print '</table>';
            print sprintf('<h2>%s</h2>', $Y[0]['name']);
            print '<table>';
            print '<tr>';
            print '<th>Date</th>';
            print '<th>Price</th>';
            print '<th>Signal</th>';
            print '<th>Processed</th>';        
            print '</tr>';
            foreach($Y as $zz)
            {
                print '<tr>';
                print sprintf('<td>%s</td>', $zz['date']);
                $q = sprintf('SELECT `date`,`close` FROM `Main`.`Candles` WHERE `instrument_id`=%d AND `freq`=1 AND `date`<="%s" ORDER BY `date` DESC LIMIT 1', $zz['asset_id'], $zz['date']);
                $P = $db->executeQuery($q);
                $lastitem = $P[0];
                print sprintf('<td>%.2f</td>', $lastitem['close']);
                print sprintf('<td>%s</td>', int_to_signal($zz['side']) );
                print sprintf('<td>%s</td>', $zz['processed'] < 0 ? 'Quasitrader' : 'Live');
                print '</tr>';
            }
            print '</table>';
    }
else
    print "No signals yet in this portfolio";

?>
