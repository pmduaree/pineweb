<?php
session_start();
$allowed_users = array('iztok','david','abraham','pedro','senatore');

$basedir = dirname(__FILE__);
require_once($basedir . '/../include/auth.php');
$login = new LoginInfo();
if (!in_array($login->username() , $allowed_users) ) {
    print 'You do not have permission to submit an optimization task.';
    exit();
}


// add a new intro into the database.

// require_once($basedir . '/../include/sql_db.php');

//print_r($_GET);

$date1 = str_replace("'","",$_GET['date1']);
$date2 = str_replace("'","",$_GET['date2']);
$cmd = sprintf('/tmp/Vector_Optimization.exe -s %s -M %s -r %s --from="%s" --to="%s"', 
	$_GET['strategyid'], $_GET['money'], $_GET['resolution'], $date1, $date2);


//print $cmd;
print shell_exec($cmd);
// $query = sprintf('INSERT INTO Main.SignalOptimization (portfolio_id, date_from, date_to, trials, offset, risk) VALUES 
// 	(%d,\'%s\',\'%s\',%d, %d, \'%s\');', $_GET['portfolioid'], $date1, $date2, $_GET['trials'], $_GET['offset'], $_GET['risk']);
// //print $query;


// $db = new sql_db('Main');
// $db->mysqli->real_query($query);
?>
