<?php
session_start();
$basedir = dirname(__FILE__);

require_once($basedir . '/../include/auth.php');

$allowed_users = array('iztok');

$login = new LoginInfo();
if (!in_array($login->username(), $allowed_users)) {
    print 'You are not allowed to remove the live asset';
    exit();
}

require_once($basedir . '/../include/sql_db.php');


$liveid =$_GET['liveid'];
$db = new sql_db('');
$query = sprintf("DELETE FROM `Main`.`Trading` WHERE `id`=%d", $liveid);
$db->realQuery($query);
$query = sprintf("DELETE FROM `Main`.`Portfolio` WHERE `instrument_id`=%d", $liveid);
$db->realQuery($query);

?>

