<?php
$basedir = dirname(__FILE__);
require_once($basedir . '/../include/auth.php');


$allowed_users = array('iztok','ryan','abraham');
$login = new LoginInfo();
if (!in_array($login->username(), $allowed_users)) {
    print 'Permission denied.';
    exit();
}


require_once($basedir . '/../include/sql_db.php');

$liveid =$_GET['liveid'];
$asset_id=$_GET['asset'];
$portfolio_id = $_GET['portfolio'];
$traderpar_id = $_GET['trparam'];

$db = new sql_db('');

$query = '';
if ($liveid) {
    $query = sprintf("UPDATE `Main`.`Trading` SET `asset_id`=%d, `strategyportfolio_id`=%d, `traderpar_id`=%d WHERE `id`=%d",
                     $asset_id, $portfolio_id, $traderpar_id, $liveid);

} else {
    $query = sprintf("INSERT INTO `Main`.`Trading` (`asset_id`,`strategyportfolio_id`,`traderpar_id`,`disabled`) VALUES(%d,%d,%d,0)", 
                     $asset_id, $portfolio_id, $traderpar_id);
}
$db->realQuery($query);
?>
