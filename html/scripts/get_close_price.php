<?php

/*session_start();
error_reporting(E_ALL);
ini_set('display_errors', True);*/

require_once('../include/highchart.php');
require_once('../include/sql_rawdata.php');
require_once('../include/sql_instruments.php');


class ClosePriceJSON
{
    /**
     *
     * Helper method to create one row of data.
     * @param string $dateStr - string for date
     * @param number $close - close amount
     */
    function helper_array($dateStr,$close) 
    {
        $data1 = array();
        $data1[] = DateTime::createFromFormat('Y-m-d H:i:s', $dateStr)->getTimestamp()*1000;
        $data1[] = $close;
        return $data1;
    }
    function __construct($DATA)
    {
        $series = array();
        //$series = '';
        foreach($DATA as $x) {
            $series[] = $this->helper_candle($x['date'], $x['close']);
            //$series .= $this->helper_candle($x['date'], $x['open'], $x['high'], $x['low'], $x['close']);
        }
        //$series = "[". $series."]";
        $this->graph = $this->generate2DArray($series);
        //new HighChart($container, $graph_name, $series, 'candlestick');
    }
    public static function generate2DArray($data) {
        array_multisort($data);
        $result = "";
        $result = $result."[";
        foreach ($data as $row) {
            $result = $result."[";
            foreach ($row as $column) {
                $result = $result.$column.", ";
            }
            $result = $result."],\n";
        }
        $result = $result."]";

        return $result;
    }
    function __toString()
    {
        $s = '';
        $s .= $this->graph;
        return $s;

    }

}

$instrument_id = isset($_GET['id']) ? $_GET['id'] : 36;
$freq = isset($_GET['freq']) ? $_GET['freq'] : 60;
$limit = isset($_GET['limit']) ? $_GET['limit'] : 1000;
$name = isset($_GET['name']) ? $_GET['name'] : "Candle chart";
$callback = isset($_GET['callback']) ? $_GET['callback'] : "?";

if ($limit > 2000) $limit = 2000;

// get the data
$sql_rd = new sql_rawdata('tix');
$fields = array('date','close');
$DATA = $sql_rd->get_data($instrument_id, $fields, $limit, $freq);

//create the graph
$g1 = new ClosePriceJSON($DATA);

//scripts so it can handle the highcharts
//print '<script src="../js/jquery-1.10.2.min.js"></script>';
//print '<script src="../js/main.js"></script>';
//print '<link rel="stylesheet" href="../css/main.css" media="screen" style="text/css">';

/*print '<div id="candlechart1">';

print '</div>';*/
print $callback. '('. $g1 .')';
?>

