<?php
$basedir = dirname(__FILE__);
$include_dir = $basedir . '/../include';


require_once($include_dir . '/auth.php');
$allowed_users = array('iztok', 'abraham','ryan','pedro','senatore','david');
$login = new LoginInfo();
if (!in_array($login->username(), $allowed_users)) {
    print 'Permission denied.';
    exit();
}


require_once($include_dir . '/sql_db.php');
$query = sprintf('UPDATE `Main`.`Instruments` SET `disabled` = 1 WHERE `id`=%d', $_GET['id']);
#print $query;
$db = new sql_db('');
$db->realQuery($query);
?>
