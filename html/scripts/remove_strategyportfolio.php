<?php
require_once('../include/auth.php');
$login = new LoginInfo();
$allowed_users = array('iztok','pedro','abraham','senatore');
if (!in_array($login->username(), $allowed_users)) {
    print 'Sorry. You are not allowed to delete strategies.';
    exit();
}


$portfolioid = $_GET['id'];
require_once('../include/sql_db.php');
$db = new sql_db('');

$query = sprintf('DELETE FROM `Main`.`Strategies` WHERE `id`=%d', $portfolioid);
$db->realQuery($query);

?>
