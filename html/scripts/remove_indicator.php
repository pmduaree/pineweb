<?php

session_start();
require_once('../include/auth.php');
$allowed_users = array('iztok');
$login = new LoginInfo();
if (!in_array($login->username(), $allowed_users) ) {
    print "Permission denied";
    exit();
}


$strategyid = $_GET['id'];

require_once('../include/sql_db.php');
$db = new sql_db('');

$query = sprintf('DELETE FROM `Main`.`Strategies` WHERE `id`=%d', $strategyid);
$db->realQuery($query);

?>
