<?php 
session_start();

require_once('../include/auth.php');
$login = new LoginInfo();

$allowed_users = array('iztok','pedro','david','senatore');
if (!$login->is_allowed($allowed_users)) {
    echo "you are not allowed to delete a task";
    exit;
}

require_once('../include/sql_db.php');
$db = new sql_db('');

$taskid = $_GET['id'];

$query = sprintf('DELETE FROM `Main`.`Portfolio` WHERE `portfolio_id`=%d', $taskid);
//print $query;
$db->realQuery($query);

?>
