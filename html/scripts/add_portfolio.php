<?php

error_reporting(E_ALL);
ini_set('display_errors', True);

$basedir = dirname(__FILE__);
$include_dir = $basedir . '/../include';

require_once($include_dir . '/auth.php');

$allowed_users = array('iztok', 'abraham','ryan','pedro','david','senatore');
$login = new LoginInfo();
if (!in_array($login->username(), $allowed_users)) {
    print 'Permission denied.';
    exit();
}

require_once($include_dir . '/sql_db.php');

$db = new sql_db('');

$assets = explode(",", $_GET['assets']);
//print_r($assets);

$next_index = $db->executeQuery("SELECT MAX(portfolio_id)+1 as next_index FROM Main.Portfolio");
$next_index = $next_index[0]['next_index'];

$query = 'INSERT INTO `Main`.`Portfolio` (`portfolio_id`,`instrument_id`,`order`) VALUES ';
$order = 1;
foreach ($assets as $X)
	$query .= sprintf('(%d,%d,%d),', $next_index, $X, $order++);

$query = substr($query, 0, -1).";";
//print $query;
$db->realquery($query);
?>
