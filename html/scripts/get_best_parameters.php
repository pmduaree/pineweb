<?php

error_reporting(E_ALL);
ini_set('display_errors', True);

include ("../include/sql_db.php");

$id = $_GET['id'];

function getTable($title,$str, $id)
{
	$query = sprintf("SELECT O.id AS id2, P.id, P.freq, P.Params, P.%s FROM Main.OptimizationParamTuples AS P
	 JOIN Main.OptimizationOrders AS O ON O.id = P.OptOrder_id
	 WHERE P.OptOrder_id = %d ORDER BY %s DESC LIMIT 0,2",
	 $str, $id, $str);
	$db = new sql_db('');

	$result = $db->executeQuery($query);
	$s = "";
	$s .= sprintf ('<h2>Max %s</h2>', $title);
	$s .= '<table>';
	$s .= '<tr>';
	$s .= '<th>Id</th>';
	$s .= '<th>Freq</th>';
	$s .= '<th>Params</th>';
	$s .= sprintf ('<th>%s</th>', $title);
	$s .= '<th></th>';

	$s .= '</tr>';

	foreach($result as $R)
	{
		$s .='<tr>';
		$onclick = sprintf("selectedParams(%s,%s)", $R['id2'] ,$R['Params']);
		$s .= sprintf('<td>%s</td>', $R['id']);
		$s .= sprintf('<td>%s</td>', $R['freq']);
		$s .= sprintf('<td>%s</td>', $R['Params']);
		$s .= sprintf('<td>%s</td>', $R[$str]);
		$s .= sprintf('<td><img src=/img/Symb_add.png onclick=%s></td>', $onclick);
		$s .='</tr>';
	}
	$s .=  '</table>';
	print $s;
}

print '<div>';

getTable("Profit", 'Profit', $id);
print '<hr>';
getTable("Success Rate", 'S_Rate', $id);
print '<hr>';
getTable("Number of Trades", 'NTrades', $id);

print "</div>";

?>