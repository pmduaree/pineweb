<?php

//session_start();
/*error_reporting(E_ALL);
ini_set('display_errors', True);*/

require_once('../include/highchart.php');
require_once('../include/sql_db.php');


class MyShowGraph
{
    /**
     *
     * Helper method to create one row of data.
     * @param string $dateStr - string for date
     * @param number $open - open amount
     * @param number $high - high amount
     * @param number $low - low amount
     * @param number $close - close amount
     */

    function helper_candle($dateStr, $open, $high, $low, $close) 
    {
        $data1 = array();
        $date = DateTime::createFromFormat('Y-m-d H:i:s', $dateStr, new DateTimeZone("UTC"));
        //$date = new DateTime($dateStr, new DateTimeZone("UTC"))
        // print $dateStr;
        // $date->setTimezone(new DateTimeZone("UTC"));
        $data1[] = $date->getTimestamp()*1000;
        $data1[] = $open;
        $data1[] = $high;
        $data1[] = $low;
        $data1[] = $close;
        return $data1;
        /*return sprintf("[%s,%f,%f,%f,%f],",
         DateTime::createFromFormat('Y-m-d H:i:s', $dateStr)->getTimestamp()*1000, $open, $high, $low, $close);*/
    }


	
    function __construct($graph_name, $container ,$DATA)
    {
        $series = array();
        //$series = '';
        foreach($DATA as $x) {
            $series[] = $this->helper_candle($x['date'], $x['open'], $x['high'], $x['low'], $x['close']);
            //$series .= $this->helper_candle($x['date'], $x['open'], $x['high'], $x['low'], $x['close']);
        }
        //$series = "[". $series."]";
        $this->graph = $this->generate2DArray($series);
        //new HighChart($container, $graph_name, $series, 'candlestick');
    }
    public static function generate2DArray($data) {
        array_multisort($data);
        $result = "";
        $result = $result."[";
        foreach ($data as $row) {
            $result = $result."[";
            foreach ($row as $column) {
                $result = $result.$column.", ";
            }
            $result = $result."],\n";
        }
        $result = $result."]";

        return $result;
    }
    function __toString()
    {
        $s = '';
        $s .= $this->graph;
        return $s;

    }

}

$instrument_id = isset($_GET['id']) ? $_GET['id'] : 1;
$limit = isset($_GET['limit']) ? $_GET['limit'] : 10000;
$name = isset($_GET['name']) ? $_GET['name'] : "Candle chart";
$callback = isset($_GET['callback']) ? $_GET['callback'] : "?";

if ($limit > 2000) $limit = 2000;

// get the data
$db = new sql_db('');
$DATA = $db->execute(sprintf('SELECT `date`,`open`,`high`,`low`,`close` FROM `Main`.`Candles` WHERE instrument_id=%d ORDER BY `date` DESC LIMIT 1000', $instrument_id));

//create the graph
$g1 = new MyShowGraph($name, "candlechart", $DATA);

//scripts so it can handle the highcharts
//print '<script src="../js/jquery-1.10.2.min.js"></script>';
//print '<script src="../js/main.js"></script>';
//print '<link rel="stylesheet" href="../css/main.css" media="screen" style="text/css">';

/*print '<div id="candlechart1">';

print '</div>';*/
print $callback. '('. $g1 .')';
?>

