<?php

require_once('../include/auth.php');

$login = new LoginInfo();

$allowed_users = array('iztok','abraham','pamela','ryan','pedro','david','anupam','elim','senatore');

if (!in_array($login->username(), $allowed_users)) {
    print 'The access to order book is not enabled. Please contact iztok.';
    exit();
}

$assets = $_GET['asset'];
//print $assets;
//print $asset === 0;
if ($assets === 0) {
    print 'No asset given';
    exit();
}

require_once('../include/sql_db.php');
$db = new sql_db('');

$assets_array = split(',',$assets);
//print_r($assets_array);

foreach($assets_array as $asset)
{
    print sprintf('<h2>%s</h2>', $asset);
    $q = sprintf('SELECT * FROM `Trading`.`Orders` WHERE `asset_name`="%s" AND order_status = 2 ORDER BY `TS_Open` DESC', $asset);
    //print $q;
    $Z = $db->executeQuery($q);
    if (count($Z)) {
        print '<table>';
        print '<tr>';
        //print '<th>quantity</th>';
        print '<th>TS open</th>';
        print '<th>price open</th>';
        print '<th>TS close</th>';
        print '<th>price close</th>';
        print '<th>profit</th>';
        print '</tr>';
        $total_profit = 0;
        foreach($Z as $trade) {
            print '<tr>';
            //print sprintf('<td>%d</td>', $trade['quantity']);
            print sprintf('<td>%s</td>', $trade['TS_open']);
            print sprintf('<td>%.3f</td>', $trade['price_open']);
            print sprintf('<td>%s</td>', $trade['TS_close']);
            print sprintf('<td>%.3f</td>', $trade['price_close']);

            //$q = sprintf('SELECT * FROM `Trading`.`Orders` WHERE `asset_id`="%s" ORDER BY `TS_pushed` DESC', $asset);
            //$Z = $db->executeQuery($q);
            if($trade['open_side'] == "Buy")
                $profit = $trade['price_close'] - $trade['price_open'];
            else
                $profit = $trade['price_open'] - $trade['price_close'];
            $total_profit += $profit;
            print sprintf('<td>%.3f</td>', $profit);
            print '</tr>';
        }
        print '<tr>';
        print '<td>P&L</td>';
        print '<td></td>';
        print '<td></td>';
        print '<td></td>';
        print sprintf('<td>%.3f</td>', $total_profit);
        print '</tr>';
        print '</table>';
    } else {
        print 'No trades found.';
    }
}
?>
