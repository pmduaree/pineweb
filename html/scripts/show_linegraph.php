<?php

//session_start();
/*error_reporting(E_ALL);
ini_set('display_errors', True);*/

//require_once('../include/highchart.php');
require_once('../include/sql_db.php');


class ParseLineGraphData
{
    /**
     *
     * Helper method to create one row of data.
     * @param string $dateStr - string for date
     * @param number $open - open amount
     * @param number $high - high amount
     * @param number $low - low amount
     * @param number $close - close amount
     */

    function helper_candle($dateStr, $data) 
    {
        $data1 = array();
    	//$str = substr($dateStr, 0, -3);
        $data1[] = DateTime::createFromFormat('Y-m-d H:i:s', $dateStr, new DateTimeZone("UTC"))->getTimestamp()*1000;
        //print $data." ";
        $data1[] = $data;
        return $data1;
        /*return sprintf("[%s,%f,%f,%f,%f],",
         DateTime::createFromFormat('Y-m-d H:i:s', $dateStr)->getTimestamp()*1000, $open, $high, $low, $close);*/
    }

    function __construct($DATA, $column)
    {
        $series = array();
        //$series = '';
        //print_r($DATA);
        foreach($DATA as $x) {
            //print_r($x);
            $series[] = $this->helper_candle($x['date'],$x[$column]);
        }
        //$series = "[". $series."]";
        $this->graph = $this->generate2DArray($series);
        //new HighChart($container, $graph_name, $series, 'candlestick');
    }
    public static function generate2DArray($data) {
        array_multisort($data);
        $result = "";
        $result = $result."[";
        foreach ($data as $row) {
            $result = $result."[";
            foreach ($row as $column) {
                $result = $result.$column.", ";
            }
            $result = $result."],\n";
        }
        $result = $result."]";

        return $result;
    }
    function __toString()
    {
        $s = '';
        $s .= $this->graph;
        return $s;

    }

}
$column = isset($_GET['column']) ? $_GET['column'] : 'midprice';
$table = isset($_GET['table']) ? $_GET['table'] : 'Tick';
$instrument_id = isset($_GET['id']) ? $_GET['id'] : 1;
$limit = isset($_GET['limit']) ? $_GET['limit'] : 10000;
$callback = isset($_GET['callback']) ? $_GET['callback'] : "?";

//if ($limit > 2000) $limit = 2000;

// get the data
$db = new sql_db('');
$query = sprintf('SELECT date, %s FROM `Main`.`%s` WHERE instrument_id=%d ORDER BY `date` DESC LIMIT %d',
    $column,$table, $instrument_id, $limit);
//print $query;
//print_r($DATA)
$DATA = $db->execute($query);
//create the graph
$g1 = new ParseLineGraphData($DATA, $column);

print $callback. '('. $g1 .')';
?>

