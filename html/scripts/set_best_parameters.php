<?php

error_reporting(E_ALL);
ini_set('display_errors', True);

$basedir = dirname(__FILE__);
$include_dir = $basedir . '/../include';

require_once($include_dir . '/auth.php');

$allowed_users = array('iztok', 'abraham','ryan','pedro','david','senatore');
$login = new LoginInfo();
if (!in_array($login->username(), $allowed_users)) {
    print 'Permission denied.';
    exit();
}

require_once($include_dir . '/sql_db.php');

$db = new sql_db('');
$id = $_GET['id'];

$query = sprintf('UPDATE Main.OptimizationOrders SET Status = 3 WHERE id = %d ',  $id);
$db->realquery($query);

$query = sprintf("SELECT S.SignalerEngine_id FROM Main.Strategies AS S
	 JOIN Main.OptimizationOrders AS O ON O.Strategy_id = S.id WHERE O.id = %d" , $id);
$id = $db->executeQuery($query);
$id = $id[0]['SignalerEngine_id'];

$query = sprintf('UPDATE Main.SignalerEngine SET CurrentParam = %s WHERE id = %d ', $_GET['params'], $id);
$db->realquery($query);

?>
