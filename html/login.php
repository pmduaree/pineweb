<?php
session_start();
require_once('include/auth.php');
if ($_POST['login_submit']) {
    // process the login infor,ation.
    $user = $_POST['login_user'];
    $pass = $_POST['login_pass'];
    $logininfo = new LoginInfo();
    $succ = $logininfo->login($user, $pass);
    if ($succ) {
        header('Location: /');
        exit();
    } else {
        print '<script>alert("Login Failed. Maybe try again..."); window.history.back();</script>';
        //header('Location: /login.php');
        exit();
    }
    //die();
}

class LoginForm
{

    function __toString()
    {
    }
}
?>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="/jquery/jquery-ui-1.10.4.css">
<link rel="stylesheet" type="text/css" href="css/oleum.css">
<link rel="icon" type="image/png" href="/img/favicon.png" />
<script src="/jquery/jquery-1.10.2.js"></script>
<script src="/jquery/jquery-ui-1.10.4.js"></script>
<title>Pine Trading Systems</title>
</head>
<body>

<style>
html {
    height: 100%
}

body {
    //background: radial-gradient(white,#995c1f);
    //background: -moz-radial-gradient(white,#995c1f);
    //background: -o-radial-gradient(white,#995c1f);
    //background: -webkit-radial-gradient(white,#995c1f);
    background: radial-gradient(white,#999999);
    background: -moz-radial-gradient(white,#999999);
    background: -o-radial-gradient(white,#999999);
    background: -webkit-radial-gradient(white,#999999);
}
</style>


<div id="login_logo_container">
    <img name="oleum_logo" id="oleumlogin_logo" src="img/logo_frontpage.png" />
    <!--<br>
    <img name="oleum_logo" src="img/pine_systems.png" />-->
</div>

        
<div id="login_credentials">
    <form action="login.php" method="POST">
        <div class="login_field">
            <input type="text" name="login_user" id="login_user" placeholder="USERNAME" />
        </div>

        <div class="login_field">
            <input type="password" name="login_pass" id="login_pass" placeholder="PASSWORD" />
        </div>
        <div class="login_field">
            <input type="submit" name="login_submit" id="login_submit" value="Log in" />
        </div>
    </form>

</div>



</body>
</html>

