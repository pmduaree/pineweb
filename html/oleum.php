<?php

session_start();

require_once('include/auth.php');

$login = new LoginInfo();
if (!$login->logged_in() ) {
    header('Location: login.php');
    die();
}

// Check if we need to log in.
?>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="/jquery/jquery-ui-1.10.4.css">
<link rel="stylesheet" type="text/css" href="css/oleum.css">
<link rel="icon" type="image/png" href="/img/favicon.png" />
<script src="/jquery/jquery-1.10.2.js"></script>
<script src="/jquery/jquery-ui-1.10.4.js"></script>
<title>Pine Trading Systems</title>
</head>
<body>

<h1 style="display: none">Pine Trading Systems</h1>


<div id="left_frame" style='z-index:90'>
<div id="oleum_logo_container">
    <img name="oleum_logo" id="oleum_logo" src="img/favicon.png" />
    <br>
    <img name="oleum_logo" src="img/pine_systems.png" />
</div>

<div id="oleum_username_container">
<div id="oleum_username">
<?php print $login->username(); ?>  
</div>
</div>

<div id="oleum_logout_container">
    <img name="oleum_logout" id="oleum_logout" src="img/logout.png" />
</div>


<div id="id_menu">
    <h2>Menu</h2>
<ul id="menu" style='position:relative;z-index:100'>

<?php


$DATA = array( 'Trading'        => array( 'oleum_autoexecution' => 'Autoexecution', 'oleum_orderbook'=>'Order Book', 'oleum_quasitrader'=>'Quasi Trader'),
               'Signals'        => array( 'oleum_signals_strategy' => 'Signals'),
               'Configuration'  => array( 'oleum_strategies' => 'Strategies',  'oleum_portfolios' => 'Portfolios' , 'pine_kalman' => 'Kalman\'s Portfolio' ,),
               'Assets'         => array( 'assetdata' => 'Candles', 'ticks' => 'Bid/Ask', 'pine_artificial' => 'Artificial Data',)
            );
                
foreach($DATA as $kD => $vD)
{
    printf('<li><a href="#">%s</a>', $kD);
    printf('<ul>');

    foreach($vD as $k => $v)
    {
        printf('<li onclick=go_to("%s")><a href="#">%s</a></li>', $k, $v);
        printf('<li>-</li>');
    }
    printf('</ul>');
    printf('</li>');
}



?>
    </ul>
</div>
</div>  <!-- left_frame -->


<div id="main_frame" style='position:relative;z-index:10'>
<div id="sub_frame"></div>
</div>

<script>

//$(function() { 
//    $(document).tooltip();
//});

function DestroyUserDialog() {
    $("#userdialog").dialog('destroy');
    $("#userdialog").remove();
}

function ChangePassword(newpass, passconfirm)
{
    if (newpass != passconfirm) {
        alert("Passwords don't match. Sorry.");
        return;
    }
    $.ajax({ type: "POST",
             url: "/scripts/update_password.php", 
             data: { password : newpass },
             dataType: "html"
            });
}

$("#oleum_username_container").click( function() {
    // open a new dialog.
    var div = $("<div id='userdialog' style='display:none'>");
    var div1 = $('<div><label for="userdialog_pass">New password</label><input id="userdialog_pass" type="password" /></div>');
    var div2 = $('<div><label for="userdialog_pass2">Confirm password</label><input id="userdialog_pass2" type="password" /></div>');
    div1.appendTo(div);
    div2.appendTo(div);
    $(div).appendTo('body');
    $(div).dialog({
        //close: function(event,ui) { DestroyUserDialog()},
        title: 'Change Password',
        modal: true,
        buttons: [ {text:'Change', click: function() { ChangePassword($("#userdialog_pass").val(), $("#userdialog_pass2").val()); $(this).dialog('close') } },
                   {text:'Cancel', click: function() { $(this).dialog('close'); } }  ]
    } );
    $(div).dialog('open');

});


function go_to(name)
{
    $('#sub_frame').html(
            "<img class='center' src='/img/loading-bar.gif'>"
        );
    var link = '/include/' + name + '.php';
    console.log(link);
    $.get(link).done( function(data)
        {
            $('#sub_frame').html(data);
        });


}


$(document).ready(function() {
    // function loadMainFrame(name_tag, reset_subframe)
    // {
    //     var link = '/include/oleum_subframe.php?subframe=' + name_tag;
    //     if (reset_subframe) {
    //         var subframe_var = 'oleum.' + name_tag + '.sel';
    //         sessionStorage.removeItem(subframe_var);
    //     }
    //     sessionStorage.setItem('oleum_main_frame', name_tag);
    //     $('#main_frame').load(link);
    //     var left_button = "leftbutton_" + name_tag;
    //     $('.left_button').css('border','1px solid white');
    //     $("#" + left_button).css("border", "1px solid black");
    // }
    // var default_mainframe = 'oleum_subframe_trading';
    // var cur_mainframe = sessionStorage.getItem('oleum_main_frame');
    // if (!cur_mainframe && typeof cur_mainframe != 'string') { cur_mainframe = default_mainframe; }
    // loadMainFrame(cur_mainframe, false);

    // $("#oleum_logo").on("click", function() { loadMainFrame(default_mainframe, true); } );
    
    // $('.left_button').click( function() {
    //     loadMainFrame(this.getAttribute('name'), true);
    // });
    $(".xdsoft_datetimepicker").remove();

    $("#menu").menu({
        "position": { my: "left top", at: "right top" }
    });

    var default_frame = "oleum_autoexecution";
    go_to(default_frame);
    $('#oleum_logout').click( function() {
        $.get("/scripts/logout.php").done( function(data) { 
            window.location = "index.php";
        });
    });



});
</script>


</body>
</html>



